const jsonServer = require("json-server");
const path = require("path");

const server = jsonServer.create();
const router = jsonServer.router(path.join(__dirname, "db.json"));
const middlewares = jsonServer.defaults();
const port = 8000;

server.use(middlewares);
server.use(jsonServer.bodyParser);

const db = require("./db.json");

server.post("/auth/token", (req, res) => {
  const { method, body } = req;

  if (method === "POST") {
    const { email, password } = body;
    const result = db.users.find(
      (user) => user.email === email && user.password === password
    );
    if (result) {
      res.sendStatus(200);
    } else {
      res.sendStatus(401);
    }
  }
});

server.use(router);
server.listen(port, () => {
  console.log("JSON Server is running");
});
