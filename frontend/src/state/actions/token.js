import {SET_JWT_TOKEN} from '../actionTypes';

/* eslint-disable */
export const setToken = token => ({
  type: SET_JWT_TOKEN,
  payload: token,
});
