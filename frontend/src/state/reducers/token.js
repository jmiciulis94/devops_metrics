import {SET_JWT_TOKEN} from '../actionTypes';

const initialState = null;

export default function tokenReducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_JWT_TOKEN:
      return action.payload;
    default:
      return state;
  }
}
