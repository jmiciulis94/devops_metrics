export default {
  '/': 'Global',
  '/help': 'Help',
  '/metrics': 'Metrics',
  '/quadrant': 'Quadrant',
  '/progress': 'Progress',
  '/projects': 'Projects',
  '/squads': 'Squads',
  '/users': 'Users',
  '/settings': 'Settings',
  '/login': 'Login',
};
