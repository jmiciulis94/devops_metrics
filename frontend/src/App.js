import React from 'react';
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import {MuiThemeProvider} from '@material-ui/core';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import {useSelector} from 'react-redux';
import Login from './pages/Login/Login';
import './App.css';
import Layout from './Layout';

const theme = createMuiTheme({
  palette: {
    primary: {main: '#a293c6'},
  },
  overrides: {
    MuiInput: {
      underline: {
        '&:hover:not($disabled):not($focused):not($error):before': {
          borderBottomColor: '#a293c6',
        },
      },
    },
    MuiOutlinedInput: {
      root: {
        '&:hover:not($disabled):not($focused):not($error) $notchedOutline': {
          borderColor: '#a293c6',
        },
      },
    },
  },
});

export default function App() {
  const token = useSelector(state => state.token);
  return (
    <>
      <MuiThemeProvider theme={theme}>
        <Router>
          <Switch>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/">
              {token || sessionStorage.getItem('token') ? <Layout /> : <Redirect to="/login" />}
            </Route>
          </Switch>
        </Router>
      </MuiThemeProvider>
    </>
  );
}
