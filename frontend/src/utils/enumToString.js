export const projectStatusToString = string => {
  switch (string) {
    case 'IN_PRODUCTION':
      return 'In production';
    case 'PRE_PRODUCTION':
      return 'Pre production';
    default:
      return string;
  }
};

export const userRoleToString = string => {
  switch (string) {
    case 'ADMIN':
      return 'Administrator';
    case 'USER':
      return 'User';
    default:
      return string;
  }
};
