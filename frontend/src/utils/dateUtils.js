export const dateTimeNow = () => {
  const today = new Date();
  const date = `${today.getFullYear()}-${String(today.getMonth() + 1).padStart(2, '0')}-${String(
    today.getDate()
  ).padStart(2, '0')}`;
  const time = `${String(today.getHours()).padStart(2, '0')}:${String(today.getMinutes()).padStart(
    2,
    '0'
  )}`;
  return `${date}T${time}`;
};

export const dateTimeNowPlusTwoDays = () => {
  const date = new Date();
  date.setDate(date.getDate() + 2);
  return `${date.getFullYear()}-${String(date.getMonth() + 1).padStart(2, '0')}-${String(
    date.getDate()
  ).padStart(2, '0')}`;
};
