import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import {Form, Formik} from 'formik';
import React from 'react';
import * as yup from 'yup';
import DialogHeader from '../../components/dialogs/DialogHeader';
import DialogFooter from '../../components/dialogs/DialogFooter';
import useStyles from './useStyles';

const CreateProject = ({open, onClose, onSubmit, squadList}) => {
  const classes = useStyles();

  const validationSchema = yup.object({
    title: yup.string().required('Project title is required'),
    squadId: yup.number().required('Squad name is required'),
    notes: yup.string().notRequired(),
  });

  const initialValues = {
    title: '',
    squadId: '',
    notes: '',
  };

  return (
    <Dialog open={open} onClose={onClose} fullWidth>
      <DialogHeader onClose={onClose} title="New project" />
      <Formik
        enableReinitialize
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({values, errors, handleChange, isSubmitting}) => (
          <Form>
            <DialogContent>
              <Grid container spacing={1}>
                <Grid item xs={12}>
                  <div className={classes.paper}>
                    <TextField
                      id="title"
                      name="title"
                      label="Project title*"
                      value={values.title}
                      onChange={handleChange}
                      error={!!errors.title}
                      helperText={errors.title}
                      fullWidth
                    />
                  </div>
                  <div className={classes.paper}>
                    <FormControl className={classes.formControl} error={!!errors.squadId} fullWidth>
                      <InputLabel htmlFor="squadId">Squad*</InputLabel>
                      <Select
                        labelId="squadId"
                        id="squadId"
                        name="squadId"
                        value={values.squadId}
                        onChange={handleChange}
                        error={!!errors.squadId}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        {squadList &&
                          squadList.map(squad => (
                            <MenuItem key={squad.id} value={squad.id}>
                              {squad.name}
                            </MenuItem>
                          ))}
                      </Select>
                      <FormHelperText style={{color: 'red'}}>{errors.squadId}</FormHelperText>
                    </FormControl>
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="notes"
                    name="notes"
                    label="Project notes"
                    value={values.notes}
                    onChange={handleChange}
                    fullWidth
                    multiline
                    rows={3}
                    rowsMax={10}
                    variant="outlined"
                    error={!!errors.notes}
                  />
                </Grid>
              </Grid>
            </DialogContent>
            <DialogFooter onClose={onClose} isSubmitting={isSubmitting} />
          </Form>
        )}
      </Formik>
    </Dialog>
  );
};

export default CreateProject;

CreateProject.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  squadList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    })
  ),
};

CreateProject.defaultProps = {
  squadList: [],
};
