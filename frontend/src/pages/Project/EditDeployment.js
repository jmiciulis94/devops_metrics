import React from 'react';
import PropTypes from 'prop-types';
import {Form, Formik} from 'formik';
import DialogContent from '@material-ui/core/DialogContent';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormLabel from '@material-ui/core/FormLabel';
import Dialog from '@material-ui/core/Dialog';
import * as yup from 'yup';
import CustomRadio from '../../components/fields/CustomRadio';
import DialogFooter from '../../components/dialogs/DialogFooter';
import DialogHeader from '../../components/dialogs/DialogHeader';
import {useDeploymentStyles} from './useStyles';

const EditDeployment = ({
  open,
  onClose,
  onSubmit,
  deployment: {id, date, notes, isDefect, leadTime, deploymentDuration},
}) => {
  const classes = useDeploymentStyles();

  const validationSchema = yup.object({
    date: yup.date().required('Deployment date is required'),
    isDefect: yup.string().required('Has defect field is required'),
    notes: yup.string().notRequired(),
    leadTime: yup
      .number()
      .integer('Lead time must be an integer')
      .moreThan(0, 'Lead time must be a positive number')
      .nullable(),
    deploymentDuration: yup
      .number()
      .integer('Deployment duration must be an integer')
      .moreThan(0, 'Deployment duration must be a positive number')
      .nullable(),
  });

  const initialValues = {
    date: date && date.replace(' ', 'T').slice(0, 16),
    isDefect,
    notes,
    leadTime,
    deploymentDuration,
    id,
  };

  return (
    <Dialog open={open} onClose={onClose} fullWidth>
      <DialogHeader title="Edit deployment" onClose={onClose} />
      <Formik
        enableReinitialize
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({values, errors, handleChange, isSubmitting}) => (
          <Form>
            <DialogContent>
              <Grid container spacing={1}>
                <Grid item xs={6}>
                  <div className={classes.paper}>
                    <TextField
                      id="date"
                      name="date"
                      label="Deployment date*"
                      type="datetime-local"
                      defaultValue={values.date}
                      onChange={handleChange}
                      error={!!errors.date}
                      helperText={errors.date}
                    />
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div className={classes.paper}>
                    <FormLabel component="label" error={!!errors.isDefect}>
                      Deployment has a bug?*
                    </FormLabel>
                    <CustomRadio name="isDefect" type="radio" value="Yes" label="Yes" />
                    <CustomRadio name="isDefect" type="radio" value="No" label="No" />
                    <FormHelperText style={{color: 'red'}}>{errors.isDefect}</FormHelperText>
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div className={classes.paper}>
                    <TextField
                      id="leadTime"
                      label="Lead time (min)"
                      name="leadTime"
                      type="number"
                      value={values.leadTime || ''}
                      onChange={handleChange}
                      error={!!errors.leadTime}
                      helperText={errors.leadTime}
                    />
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div className={classes.paper}>
                    <TextField
                      id="deploymentDuration"
                      label="Deployment duration (min)"
                      name="deploymentDuration"
                      type="number"
                      value={values.deploymentDuration || ''}
                      onChange={handleChange}
                      error={!!errors.deploymentDuration}
                      helperText={errors.deploymentDuration}
                    />
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="notes"
                    name="notes"
                    label="Deployment notes"
                    value={values.notes}
                    onChange={handleChange}
                    fullWidth
                    multiline
                    rows={3}
                    rowsMax={10}
                    variant="outlined"
                    error={!!errors.notes}
                  />
                </Grid>
              </Grid>
            </DialogContent>
            <DialogFooter onClose={onClose} isSubmitting={isSubmitting} />
          </Form>
        )}
      </Formik>
    </Dialog>
  );
};

EditDeployment.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  deployment: PropTypes.shape({
    id: PropTypes.number,
    date: PropTypes.string,
    notes: PropTypes.string,
    isDefect: PropTypes.string,
    leadTime: PropTypes.number,
    deploymentDuration: PropTypes.number,
  }),
};
EditDeployment.defaultProps = {
  deployment: {
    id: null,
    date: '',
    isDefect: '',
    notes: '',
    leadTime: null,
    deploymentDuration: null,
  },
};

export default EditDeployment;
