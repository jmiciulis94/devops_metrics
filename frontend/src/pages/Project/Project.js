import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import {CircularProgress, Typography} from '@material-ui/core';
import React, {createRef, useEffect, useState} from 'react';
import {useHistory, useParams} from 'react-router';
import EditIcon from '@material-ui/icons/Edit';
import _ from 'lodash';
import {GetApp} from '@material-ui/icons';
import ProgressChart from '../../components/ProgressChart';
import CustomFilter from '../../components/CustomFilter';
import ResponseSnackbar from '../../components/ResponseSnackbar';
import ProjectDetails from './ProjectDetails';
import CreateDeployment from './CreateDeployment';
import Table from '../../components/Table';
import deploymentGridColumns from './deploymentGridColumns';
import DeleteDialog from '../../components/dialogs/DeleteDialog';
import EditDeployment from './EditDeployment';
import {useProjectStyles} from './useStyles';
import EditProject from './EditProject';
import {useFetch} from '../../useFetch';
import GetDeploymentToken from './GetDeploymentToken';
import errorCodes from '../../utils/errorCodes';
import getRequestUrl from '../../utils/getRequestUrl';

const tableRef = createRef();

const Project = () => {
  const {id} = useParams();
  const history = useHistory();

  const classes = useProjectStyles();

  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [openEditModal, setOpenEditModal] = useState(false);
  const [openDeleteDeploymentModal, setOpenDeleteDeploymentModal] = useState(false);
  const [openDeploymentCreate, setOpenDeploymentCreate] = useState(false);
  const [openEditDeploymentModal, setOpenEditDeploymentModal] = useState(false);
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [selectedDeployment, setSelectedDeployment] = useState({});
  const [project, setProject] = useState(null);
  const [refreshProgressChart, setRefreshProgressChart] = useState(false);
  const [snackbar, setSnackbar] = useState({
    message: '',
    color: '',
  });
  const [openTokenModal, setOpenTokenModal] = useState(false);
  const [token, setToken] = useState('');
  const [filters, setFilters] = useState({
    dateFrom: '',
    dateTo: '',
  });
  const [found, setFound] = useState(true);
  const [squadList, setSquadList] = useState([]);

  const getProjectData = async () => {
    await useFetch(`/projects/${id}`, 'GET')
      .then(response => {
        if (response.status === 404) {
          throw Error();
        }
        return response;
      })
      .then(response => response.json())
      .then(data => setProject(data))
      .catch(() => {
        setProject(null);
        setFound(false);
      });
  };
  const getSquadList = async () => {
    if (openEditModal) {
      const url = '/squads';
      await useFetch(url, 'GET')
        .then(data => data.json())
        .then(data => {
          setSquadList(data);
        });
    }
  };
  useEffect(() => {
    getSquadList();
  }, [openEditModal]);

  useEffect(() => {
    getProjectData();
  }, []);

  const handleRefreshData = () => {
    getProjectData();
  };

  const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };

  const handleOpenEditModal = () => {
    setOpenEditModal(true);
  };

  const handleCloseEditModal = () => {
    setOpenEditModal(false);
  };

  const handleEditSubmit = (values, {setSubmitting}) => {
    setSubmitting(true);
    useFetch(`/projects/${values.id}`, 'PUT', values).then(response => {
      if (response.status >= 200 && response.status < 300) {
        setSubmitting(false);
        setOpenEditModal(false);
        setSnackbar({
          message: `The project was edited successfully.`,
          color: 'success',
        });
        setOpenSnackbar(true);
        handleRefreshData();
      } else {
        response
          .json()
          .then(data => {
            throw Error(JSON.stringify(data));
          })
          .catch(err => {
            const errorMessage = err.message.includes(errorCodes.ALREADY_EXISTS)
              ? `The project could not be edited. This project title already exists.`
              : `The project could not be edited.`;
            setSnackbar({
              message: errorMessage,
              color: 'error',
            });
            setOpenSnackbar(true);
          });
        setSubmitting(false);
      }
    });
  };

  const handleCloseDeleteModal = () => {
    setOpenDeleteModal(false);
  };

  const handleOpenDeleteModal = () => {
    setOpenDeleteModal(true);
  };

  const handleOpenDeploymentCreate = () => {
    setOpenDeploymentCreate(true);
  };

  const handleCloseDeploymentCreate = () => {
    setOpenDeploymentCreate(false);
  };

  const handleFilterSubmit = (values, {setSubmitting}) => {
    setSubmitting(true);
    setFilters({
      dateFrom: values.dateFrom,
      dateTo: values.dateTo,
    });
    tableRef.current.onQueryChange();
    setSubmitting(false);
  };

  const handleFilterClear = resetForm => {
    resetForm({});
    setFilters({
      dateTo: '',
      dateFrom: '',
    });
    tableRef.current.onQueryChange();
  };

  const handleDeploymentCreate = (values, {setSubmitting}) => {
    const valuesToReturn = {...values, isDefect: values.isDefect === 'Yes'};
    setSubmitting(true);
    useFetch(`/projects/${id}/deployments`, 'POST', valuesToReturn).then(response => {
      if (response.status >= 200 && response.status < 300) {
        setSubmitting(false);
        setOpenDeploymentCreate(false);
        setSnackbar({
          message: `The deployment was added successfully.`,
          color: 'success',
        });
        setOpenSnackbar(true);
        handleRefreshData();
        tableRef.current.onQueryChange();
        setRefreshProgressChart(!refreshProgressChart);
      } else {
        response
          .json()
          .then(data => {
            throw Error(JSON.stringify(data));
          })
          .catch(() => {
            setSnackbar({
              message: `The deployment could not be added.`,
              color: 'error',
            });
            setOpenSnackbar(true);
          });
        setSubmitting(false);
      }
    });
  };

  const handleDeleteSubmit = (values, {setSubmitting}) => {
    setSubmitting(true);
    useFetch(`/projects/${values}`, 'DELETE')
      .then(() => {
        history.push('/projects');
      })
      .catch(() => {
        setSnackbar({
          message: 'The project could not be deleted.',
          color: 'error',
        });
        setOpenSnackbar(true);
      });
    setSubmitting(false);
    setOpenDeleteModal(false);
  };

  const handleOpenEditDeploymentModal = () => {
    setOpenEditDeploymentModal(true);
  };

  const handleSelectData = data => {
    setSelectedDeployment(data);
  };

  const handleCloseEditDeploymentModal = () => {
    setOpenEditDeploymentModal(false);
  };

  const handleEditDeploymentSubmit = (values, {setSubmitting}) => {
    setSubmitting(true);
    const valuesToReturn = {...values, isDefect: values.isDefect === 'Yes'};
    useFetch(`/projects/${id}/deployments/${valuesToReturn.id}`, 'PUT', valuesToReturn).then(
      response => {
        if (response.status >= 200 && response.status < 300) {
          setSubmitting(false);
          setOpenEditDeploymentModal(false);
          setSnackbar({
            message: `The deployment was edited successfully.`,
            color: 'success',
          });
          setOpenSnackbar(true);
          tableRef.current.onQueryChange();
          setRefreshProgressChart(!refreshProgressChart);
        } else {
          response
            .json()
            .then(data => {
              throw Error(JSON.stringify(data));
            })
            .catch(() => {
              setSnackbar({
                message: `Deployment could not be edited.`,
                color: 'error',
              });
              setOpenSnackbar(true);
            });
          setSubmitting(false);
        }
      }
    );
  };

  const handleCloseDeleteDeploymentModal = () => {
    setOpenDeleteDeploymentModal(false);
  };

  const handleOpenDeleteDeploymentModal = () => {
    setOpenDeleteDeploymentModal(true);
  };

  const handleDeleteDeploymentSubmit = (values, {setSubmitting}) => {
    setSubmitting(true);
    useFetch(`/projects/${id}/deployments/${values}`, 'DELETE').then(response => {
      if (response.status >= 200 && response.status < 300) {
        setSubmitting(false);
        setOpenDeleteDeploymentModal(false);
        setSnackbar({
          message: `The deployment was deleted successfully.`,
          color: 'success',
        });
        setOpenSnackbar(true);
        handleRefreshData();
        setRefreshProgressChart(!refreshProgressChart);
        if (tableRef.current.state.data.length - 1 === 0 && tableRef.current.state.query.page > 0) {
          tableRef.current.state.query.page -= 1;
        }
        tableRef.current.onQueryChange();
      } else {
        response
          .json()
          .then(data => {
            throw Error(JSON.stringify(data));
          })
          .catch(() => {
            setSnackbar({
              message: `The deployment could not be deleted.`,
              color: 'error',
            });
            setOpenSnackbar(true);
          });
        setSubmitting(false);
      }
    });
  };

  const handleOpenTokenModal = () => {
    useFetch(`/projects/${id}/deployment-token`, 'GET')
      .then(response => {
        return response.text();
      })
      .then(text => setToken(text))
      .then(setOpenTokenModal(true));
  };

  const handleCloseTokenModal = () => {
    setOpenTokenModal(false);
  };

  const [hasLoaded, setHasLoaded] = useState(false);

  const handleLoad = didLoad => {
    setHasLoaded(didLoad);
  };

  const deploymentData = async query => {
    let url = `/projects/${id}/deployments?limit=${query.pageSize}&page=${query.page}`;
    url = getRequestUrl(url, filters);
    const response = await useFetch(url, 'GET');
    const jsonData = await response.json();
    const {headers} = response;
    return {
      data: jsonData.map(deployment => {
        return {...deployment, isDefect: deployment.isDefect ? 'Yes' : 'No'};
      }),
      page: query.page,
      totalCount: parseInt(headers.get('X-Total-Count') || 0, 10),
    };
  };

  if (project && found) {
    return (
      <>
        <Grid container justify="flex-start" spacing={2} alignItems="flex-start">
          <Grid item container justify="flex-start" sm={12} md={5}>
            <Grid item>
              <Typography className={classes.title}>Project Details</Typography>
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                color="primary"
                startIcon={<EditIcon />}
                onClick={handleOpenEditModal}
                disabled={_.isEmpty(project)}
              >
                Edit
              </Button>
              <EditProject
                open={openEditModal}
                onClose={handleCloseEditModal}
                onSubmit={handleEditSubmit}
                project={project}
                squadList={squadList}
              />
            </Grid>
          </Grid>
          <Grid item container justify="flex-end" xs={12} md={7} spacing={1}>
            <Grid item md="auto" xs={12}>
              <Button
                variant="contained"
                color="primary"
                startIcon={<AddIcon />}
                onClick={handleOpenDeploymentCreate}
                disabled={_.isEmpty(project)}
              >
                Create Deployment
              </Button>
            </Grid>
            <Grid item md="auto" xs={12}>
              <Button
                variant="contained"
                color="primary"
                startIcon={<GetApp />}
                disabled={_.isEmpty(project)}
                onClick={handleOpenTokenModal}
              >
                Generate deployment token
              </Button>
            </Grid>
          </Grid>
        </Grid>
        <CreateDeployment
          onSubmit={handleDeploymentCreate}
          onClose={handleCloseDeploymentCreate}
          open={openDeploymentCreate}
        />

        <GetDeploymentToken onClose={handleCloseTokenModal} open={openTokenModal} token={token} />
        <Grid item>
          <ProjectDetails key={project.id} project={project} />
        </Grid>
        <Grid item className={classes.space}>
          <CustomFilter
            onClear={handleFilterClear}
            onSubmit={handleFilterSubmit}
            title="Select Date/Time"
            hasLoaded={hasLoaded}
          />
        </Grid>

        <Box mt={5} mb={5}>
          <ProgressChart
            projectId={parseInt(id, 10)}
            filters={filters}
            hasLoaded={hasLoaded}
            onLoad={handleLoad}
            handleRefresh={refreshProgressChart}
          />
        </Box>
        <Box mt={5} mb={5}>
          <Table
            title="Deployments"
            columns={deploymentGridColumns}
            tableRef={tableRef}
            data={deploymentData}
            openEditModal={handleOpenEditDeploymentModal}
            showEditAction
            handleData={handleSelectData}
            openDeleteModal={handleOpenDeleteDeploymentModal}
            showDeleteAction
          />
        </Box>
        <EditDeployment
          onSubmit={handleEditDeploymentSubmit}
          onClose={handleCloseEditDeploymentModal}
          open={openEditDeploymentModal}
          deployment={selectedDeployment}
        />
        <DeleteDialog
          onSubmit={handleDeleteDeploymentSubmit}
          onClose={handleCloseDeleteDeploymentModal}
          id={selectedDeployment.id}
          open={openDeleteDeploymentModal}
          entityName="this deployment"
        />
        <ResponseSnackbar
          open={openSnackbar}
          message={snackbar.message}
          success={snackbar.color}
          onClose={handleCloseSnackbar}
        />
        <Grid container justify="flex-end">
          <Button
            variant="contained"
            color="primary"
            startIcon={<DeleteIcon />}
            onClick={handleOpenDeleteModal}
            disabled={_.isEmpty(project)}
          >
            Delete project
          </Button>

          <DeleteDialog
            onSubmit={handleDeleteSubmit}
            onClose={handleCloseDeleteModal}
            open={openDeleteModal}
            id={project.id}
            entityName={project.title}
          />
        </Grid>
      </>
    );
  }
  if (!found) {
    return <h2>Project not found.</h2>;
  }
  return (
    <Grid container alignItems="center" justify="center" spacing={2}>
      <CircularProgress />
    </Grid>
  );
};

export default Project;
