import React, {useState} from 'react';
import Dialog from '@material-ui/core/Dialog';
import PropTypes from 'prop-types';
import DialogContent from '@material-ui/core/DialogContent';
import {Button, DialogActions} from '@material-ui/core';
import {FileCopy} from '@material-ui/icons';
import DialogHeader from '../../components/dialogs/DialogHeader';
import {useDialogFooterStyles} from './useStyles';
import ResponseSnackbar from '../../components/ResponseSnackbar';

const GetDeploymentToken = ({open, onClose, token}) => {
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [snackbar, setSnackbar] = useState({
    message: '',
    color: '',
  });
  const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };
  const classes = useDialogFooterStyles();
  const copyToClipboard = () => {
    navigator.clipboard.writeText(token);
  };
  return (
    <>
      <Dialog open={open} onClose={onClose} fullWidth>
        <DialogHeader onClose={onClose} title="Deployment token" />
        <DialogContent>
          <div style={{height: 'fit-content', overflowWrap: 'break-word', padding: '5px'}}>
            {token}
          </div>
        </DialogContent>
        <DialogActions className={classes.dialogActionButtons}>
          <Button
            variant="contained"
            color="primary"
            startIcon={<FileCopy />}
            onClick={() => {
              copyToClipboard();
              setSnackbar({
                message: `The deployment token has been copied to the clipboard.`,
                color: 'success',
              });
              setOpenSnackbar(true);
            }}
          >
            Copy
          </Button>
        </DialogActions>
      </Dialog>
      <ResponseSnackbar
        open={openSnackbar}
        message={snackbar.message}
        success={snackbar.color}
        onClose={handleCloseSnackbar}
      />
    </>
  );
};

GetDeploymentToken.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired,
};

export default GetDeploymentToken;
