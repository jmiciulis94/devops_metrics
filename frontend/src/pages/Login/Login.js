import React, {useEffect, useState} from 'react';
import {Grid, TextField, Container, Typography} from '@material-ui/core';
import {useFormik} from 'formik';
import * as yup from 'yup';
import Button from '@material-ui/core/Button';
import {useDispatch, useSelector} from 'react-redux';
import {useHistory} from 'react-router';
import useLoginStyles from './useStyles';
import {setToken} from '../../state/actions/token';
import {useFetch} from '../../useFetch';
import logo from './logo.png';

const Login = () => {
  const token = useSelector(state => state.token);
  const history = useHistory();

  useEffect(() => {
    if (token || sessionStorage.getItem('token')) {
      history.push('/');
    }
  }, [token]);

  const [hasLoginFailed, setHasLoginFailed] = useState(false);

  const dispatch = useDispatch();
  const classes = useLoginStyles();
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: yup.object({
      email: yup.string().required('Email is required').email('Invalid email address'),
      password: yup.string().required('Password is required'),
    }),
    onSubmit: values => {
      useFetch('/auth/login', 'POST', values).then(res => {
        if (res.status === 200) {
          const jwt = res.headers.get('Authorization');
          const role = res.headers.get('Role');
          res
            .json()
            .then(data => {
              const {firstName, lastName, email} = data;
              sessionStorage.setItem('firstName', firstName);
              sessionStorage.setItem('lastName', lastName);
              sessionStorage.setItem('email', email);
              sessionStorage.setItem('role', role);
            })
            .then(() => {
              sessionStorage.setItem('token', jwt);
              dispatch(setToken(jwt));
            });
        } else if (res.status === 400) {
          setHasLoginFailed(true);
        }
      });
    },
  });

  return (
    <div className={classes.root}>
      <Container maxWidth="xs">
        <div className={classes.center}>
          <form onSubmit={formik.handleSubmit} autoComplete="off">
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <table style={{marginLeft: 'auto', marginRight: 'auto'}}>
                  <thead>
                    <tr>
                      <td style={{textAlign: 'center'}}>
                        <img src={logo} alt="logo" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <Typography className={classes.typography}>DevOps Metrics</Typography>
                      </td>
                    </tr>
                  </thead>
                </table>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  id="email"
                  value={formik.values.email}
                  label="Email:"
                  fullWidth
                  onChange={formik.handleChange}
                />
              </Grid>
              {formik.touched.email && formik.errors.email ? (
                <Grid item xs={12} style={{color: 'red'}}>
                  {formik.errors.email}
                </Grid>
              ) : null}
              <Grid item xs={12}>
                <TextField
                  id="password"
                  value={formik.values.password}
                  type="password"
                  label="Password:"
                  fullWidth
                  onChange={formik.handleChange}
                />
              </Grid>
              {formik.touched.password && formik.errors.password ? (
                <Grid item xs={12} style={{color: 'red'}}>
                  {formik.errors.password}
                </Grid>
              ) : null}
              <Grid item xs={12}>
                <Button type="submit" variant="contained" className={classes.button} fullWidth>
                  Login
                </Button>
              </Grid>
              {hasLoginFailed ? (
                <Grid item xs={12} style={{color: 'red', textAlign: 'center'}}>
                  Wrong email or password. Please try again.
                </Grid>
              ) : null}
            </Grid>
          </form>
        </div>
      </Container>
    </div>
  );
};

export default Login;
