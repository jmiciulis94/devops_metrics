import {makeStyles} from '@material-ui/core/styles';

const useLoginStyles = makeStyles(theme => ({
  root: {
    backgroundColor: '#ebe2ff',
  },
  button: {
    color: theme.palette.getContrastText('#a293c6'),
    backgroundColor: '#a293c6',
  },
  center: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100vh',
  },
  typography: {
    fontSize: '1.4rem',
    textAlign: 'center',
    fontWeight: 'bold',
  },
}));

export default useLoginStyles;
