import {makeStyles} from '@material-ui/core/styles';

const useSquadStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2.5),
  },
}));

export default useSquadStyles;
