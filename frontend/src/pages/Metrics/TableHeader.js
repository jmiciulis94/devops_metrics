import React from 'react';
import PropTypes from 'prop-types';
import {FilterList, Refresh} from '@material-ui/icons';
import {Tooltip, TableHead, TableRow, TableCell, IconButton} from '@material-ui/core';
import {useHeaderStyles} from './useStyles';

const TableHeader = ({openMetricsFilter, refreshData, hasLoaded}) => {
  const headerClasses = useHeaderStyles();
  return (
    <TableHead>
      <TableRow classes={{root: headerClasses.greyBackground}}>
        <TableCell classes={{root: headerClasses.metricsFontSize}} align="left">
          Metrics
        </TableCell>
        <TableCell />
        <TableCell />
        <TableCell />
        <TableCell />
        <TableCell align="right">
          <Tooltip title="Filter list">
            <span>
              <IconButton
                aria-label="filter list"
                style={{padding: '5px'}}
                onClick={() => openMetricsFilter()}
                disabled={!hasLoaded}
              >
                <FilterList />
              </IconButton>
            </span>
          </Tooltip>
          <Tooltip title="Refresh">
            <span>
              <IconButton
                aria-label="refresh"
                style={{padding: '5px'}}
                onClick={() => refreshData()}
                disabled={!hasLoaded}
              >
                <Refresh />
              </IconButton>
            </span>
          </Tooltip>
        </TableCell>
      </TableRow>
      <TableRow classes={{root: headerClasses.purpleBackground}}>
        <TableCell align="left" classes={{root: headerClasses.bold}}>
          Project
        </TableCell>
        <TableCell align="left" classes={{root: headerClasses.bold}}>
          Squad
        </TableCell>
        <TableCell align="center" classes={{root: headerClasses.bold}}>
          Deployment frequency (#/day)
        </TableCell>
        <TableCell align="center" classes={{root: headerClasses.bold}}>
          Lead time (min)
        </TableCell>
        <TableCell align="center" classes={{root: headerClasses.bold}}>
          Deployment duration (min)
        </TableCell>
        <TableCell align="center" classes={{root: headerClasses.bold}}>
          Failure rate (%)
        </TableCell>
      </TableRow>
    </TableHead>
  );
};

TableHeader.propTypes = {
  openMetricsFilter: PropTypes.func.isRequired,
  refreshData: PropTypes.func.isRequired,
  hasLoaded: PropTypes.bool.isRequired,
};

export default TableHeader;
