import {makeStyles} from '@material-ui/core/styles';

const useUserStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2.5),
  },
}));

export default useUserStyles;
