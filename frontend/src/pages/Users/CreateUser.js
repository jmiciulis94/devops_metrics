import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import {Form, Formik} from 'formik';
import React from 'react';
import * as yup from 'yup';
import DialogHeader from '../../components/dialogs/DialogHeader';
import DialogFooter from '../../components/dialogs/DialogFooter';
import useUserStyles from './useStyles';

const CreateUser = ({open, onClose, onSubmit}) => {
  const classes = useUserStyles();
  const validationSchema = yup.object({
    firstName: yup
      .string()
      .required('First name is required')
      .trim('First name cannot be blank')
      .strict()
      .max(255, 'First name must have no more than 255 characters'),
    lastName: yup
      .string()
      .required('Last name is required')
      .trim('Last name cannot be blank')
      .strict()
      .max(255, 'Last name must have no more than 255 characters')
      .min(3, 'Last name must have no less than 3 characters'),
    email: yup.string().required('Enter your email'),
    password: yup
      .string()
      .required('Password is required')
      .trim('Password cannot be blank')
      .matches(
        /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        'Password must contain at least 8 characters, one uppercase, one number and one special case character'
      ),
  });

  const initialValues = {
    firstName: '',
    lastName: '',
    password: '',
    email: '',
  };

  return (
    <Dialog open={open} onClose={onClose} fullWidth>
      <DialogHeader onClose={onClose} title="Create user" />
      <Formik
        enableReinitialize
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({values, errors, handleChange, isSubmitting}) => (
          <Form>
            <DialogContent>
              <Grid container spacing={1}>
                <Grid item xs={12}>
                  <div className={classes.paper}>
                    <TextField
                      id="firstName"
                      name="firstName"
                      label="First name*"
                      value={values.firstName}
                      onChange={handleChange}
                      error={!!errors.firstName}
                      helperText={errors.firstName}
                      placeholder="Please enter your first name"
                      fullWidth
                    />
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <div className={classes.paper}>
                    <TextField
                      id="lastName"
                      name="lastName"
                      label="Last name*"
                      value={values.lastName}
                      onChange={handleChange}
                      error={!!errors.lastName}
                      helperText={errors.lastName}
                      placeholder="Please enter your last name"
                      fullWidth
                    />
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <div className={classes.paper}>
                    <TextField
                      id="email"
                      name="email"
                      label="Email*"
                      value={values.email}
                      onChange={handleChange}
                      error={!!errors.email}
                      helperText={errors.email}
                      placeholder="Please enter your email"
                      fullWidth
                    />
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <div className={classes.paper}>
                    <TextField
                      id="password"
                      name="password"
                      label="Password*"
                      value={values.password}
                      onChange={handleChange}
                      error={!!errors.password}
                      helperText={errors.password}
                      placeholder="Please enter your password"
                      fullWidth
                    />
                  </div>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogFooter isSubmitting={isSubmitting} onClose={onClose} />
          </Form>
        )}
      </Formik>
    </Dialog>
  );
};

export default CreateUser;

CreateUser.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};
