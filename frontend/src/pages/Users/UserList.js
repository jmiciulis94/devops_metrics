import React, {createRef, useState} from 'react';
import {Box, Button, Grid} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import {Redirect} from 'react-router';
import Table from '../../components/Table/Table';
import ResponseSnackbar from '../../components/ResponseSnackbar';
import {useFetch} from '../../useFetch';
import CreateUser from './CreateUser';
import EditUser from './EditUsers';
import DeleteDialog from '../../components/dialogs/DeleteDialog';
import {userRoleToString} from '../../utils/enumToString';

const tableRef = createRef();

const UserList = () => {
  const [openCreateModal, setOpenCreateModal] = useState(false);
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [openEditModal, setOpenEditModal] = useState(false);
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [selectUser, setSelectedUser] = useState({});
  const [snackbar, setSnackbar] = useState({
    message: '',
    color: '',
  });
  const [checkbox, setCheckbox] = useState(false);

  const check = () => {
    setCheckbox(!checkbox);
  };

  const columns = [
    {
      title: 'First Name',
      field: 'firstName',
    },
    {
      title: 'Last Name',
      field: 'lastName',
    },
    {
      title: 'Email',
      field: 'email',
    },
    {
      title: 'Role',
      field: 'role',
      render: rowData => userRoleToString(rowData.role),
    },
  ];

  const handleCloseSnackbar = reason => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };

  const handleCloseEditModal = () => {
    setOpenEditModal(false);
    setCheckbox(false);
  };

  const handleOpenEditModal = () => {
    setOpenEditModal(true);
  };

  const handleSelectedData = data => {
    setSelectedUser(data);
  };

  const handleOpenCreateModal = () => {
    setOpenCreateModal(true);
  };

  const handleCloseCreateModal = () => {
    setOpenCreateModal(false);
  };
  const handleOpenDeleteModal = () => {
    setOpenDeleteModal(true);
  };

  const handleDeleteDialogClosed = () => {
    setOpenDeleteModal(false);
  };

  const handleEditSubmit = (values, {setSubmitting}) => {
    setSubmitting(true);
    useFetch(`/users/${values.id}`, 'PUT', values).then(response => {
      if (response.status >= 200 && response.status < 300) {
        setSubmitting(false);
        setOpenEditModal(false);
        setSnackbar({
          message: `The user was edited successfully.`,
          color: 'success',
        });
        setOpenSnackbar(true);
        tableRef.current.onQueryChange();
        setCheckbox(false);
      } else {
        response
          .json()
          .then(data => {
            throw Error(JSON.stringify(data));
          })
          .catch(() => {
            setSnackbar({
              message: `The user could not be edited.`,
              color: 'error',
            });
            setOpenSnackbar(true);
          });
        setSubmitting(false);
      }
    });
  };

  const handleDeleteSubmit = (values, {setSubmitting}) => {
    setSubmitting(true);
    useFetch(`/users/${values}`, 'DELETE').then(response => {
      if (response.status >= 200 && response.status < 300) {
        setSnackbar({
          message: 'The user was deleted successfully.',
          color: 'success',
        });
        setOpenSnackbar(true);
        if (tableRef.current.state.data.length - 1 === 0 && tableRef.current.state.query.page > 0) {
          tableRef.current.state.query.page -= 1;
        }
        tableRef.current.onQueryChange();
      } else {
        response
          .json()
          .then(data => {
            throw Error(data.message);
          })
          .catch(err => {
            setSnackbar({
              message: err.message,
              color: 'error',
            });
            setOpenSnackbar(true);
          });
      }
    });
    setSubmitting(false);
    setOpenDeleteModal(false);
  };

  const handleCreateSubmit = (values, {setSubmitting}) => {
    setSubmitting(true);
    useFetch('/users', 'POST', values).then(response => {
      if (response.status >= 200 && response.status < 300) {
        setSubmitting(false);
        setOpenCreateModal(false);
        setSnackbar({
          message: `The user was added successfully.`,
          color: 'success',
        });
        setOpenSnackbar(true);
        tableRef.current.onQueryChange();
      } else {
        response
          .json()
          .then(data => {
            throw Error(JSON.stringify(data));
          })
          .catch(() => {
            setSnackbar({
              message: `The user could not be added.`,
              color: 'error',
            });
            setOpenSnackbar(true);
          });
        setSubmitting(false);
      }
    });
  };

  const data = async query => {
    const url = `/users?limit=${query.pageSize}&page=${query.page}`;
    const response = await useFetch(url, 'GET');
    const jsonData = await response.json();
    const {headers} = response;
    return {
      data: jsonData,
      page: query.page,
      totalCount: parseInt(headers.get('X-Total-Count') || 0, 10),
    };
  };

  if (sessionStorage.getItem('role') !== 'ADMIN') {
    return <Redirect to="/" />;
  }
  return (
    <>
      <Grid container justify="flex-end">
        <Box mb={5}>
          <Button
            variant="contained"
            color="primary"
            startIcon={<AddIcon />}
            onClick={handleOpenCreateModal}
          >
            Create User
          </Button>
        </Box>
      </Grid>
      <CreateUser
        onSubmit={handleCreateSubmit}
        open={openCreateModal}
        onClose={handleCloseCreateModal}
      />
      <EditUser
        onSubmit={handleEditSubmit}
        onClose={handleCloseEditModal}
        open={openEditModal}
        user={selectUser}
        checked={checkbox}
        check={check}
      />
      <DeleteDialog
        onSubmit={handleDeleteSubmit}
        onClose={handleDeleteDialogClosed}
        id={selectUser.id}
        entityName={selectUser.firstName}
        open={openDeleteModal}
      />
      <Table
        title="Users"
        columns={columns}
        tableRef={tableRef}
        data={data}
        openEditModal={handleOpenEditModal}
        handleData={handleSelectedData}
        showEditAction
        openDeleteModal={handleOpenDeleteModal}
        showDeleteAction
      />
      <ResponseSnackbar
        open={openSnackbar}
        message={snackbar.message}
        success={snackbar.color}
        onClose={handleCloseSnackbar}
      />
    </>
  );
};
export default UserList;
