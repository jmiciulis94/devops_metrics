const {makeStyles} = require('@material-ui/core');

const useTooltipSheetStyles = makeStyles({
  sheet: {
    background: '#ebe2ff',
  },
});

export default useTooltipSheetStyles;
