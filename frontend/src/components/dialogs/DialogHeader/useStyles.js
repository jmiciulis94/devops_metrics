import {makeStyles} from '@material-ui/core/styles';

const useDialogHeaderStyles = makeStyles(theme => ({
  dialogTitle: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#a293c6',
    paddingLeft: theme.spacing(4),
  },
  dialogExitButton: {
    marginLeft: 'auto',
    backgroundColor: '#a293c6',
    color: 'black',
  },
}));

export default useDialogHeaderStyles;
