import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import {Clear, FilterList} from '@material-ui/icons';
import {Form, Formik} from 'formik';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import * as yup from 'yup';
import useData from '../../useData';
import mapProjectsToOptions from '../../utils/projects';
import mapSquadsToOptions from '../../utils/squads';
import CustomMultiSelect from '../fields/CustomMultiSelect';
import useFilterStyles from './useStyles';

const CustomFilter = ({
  onClear,
  onSubmit,
  shouldDisplayProjectFilter,
  shouldDisplaySquadFilter,
  title,
  hasLoaded,
  squadList,
  projectList,
}) => {
  const classes = useFilterStyles({disabled: hasLoaded});

  let projectOptions = [];
  let squadOptions = [];

  if (!_.isEmpty(projectList)) {
    projectOptions = mapProjectsToOptions(projectList);
  } else if (shouldDisplayProjectFilter) {
    projectOptions = mapProjectsToOptions(useData('/projects'));
  }

  if (!_.isEmpty(squadList)) {
    squadOptions = mapSquadsToOptions(squadList);
  } else if (shouldDisplaySquadFilter) {
    squadOptions = mapSquadsToOptions(useData('/squads'));
  }

  const validationSchema = yup.object({
    dateFrom: yup.date().min('1900-01-01').max('2200-01-01').nullable(),
    dateTo: yup
      .date()
      .min('1900-01-01')
      .max('2200-01-01')
      .when(
        'dateFrom',
        (dateFrom, schema) =>
          dateFrom &&
          schema.min(dateFrom, 'This date must be later than the one you selected for date from')
      )
      .nullable(),
    projectIds: yup.array().of(yup.number()),
    squadIds: yup.array().of(yup.number()),
  });

  const initialValues = {
    dateFrom: '',
    dateTo: '',
    projectIds: [],
    squadIds: [],
  };

  return (
    <>
      <Formik
        enableReinitialize
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({values, errors, handleChange, isSubmitting, resetForm}) => (
          <Form className={classes.loading}>
            <Typography variant="h6" gutterBottom>
              {title}
            </Typography>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6} lg={3}>
                <TextField
                  name="dateFrom"
                  label="Date from:"
                  type="datetime-local"
                  size="small"
                  className={classes.dateField}
                  value={values.dateFrom}
                  onChange={handleChange}
                  error={!!errors.dateFrom}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  helperText={errors.dateFrom}
                  disabled={!hasLoaded}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={3}>
                <TextField
                  name="dateTo"
                  label="Date to:"
                  type="datetime-local"
                  size="small"
                  className={classes.dateField}
                  value={values.dateTo}
                  onChange={handleChange}
                  error={!!errors.dateTo}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  helperText={errors.dateTo}
                  disabled={!hasLoaded}
                />
              </Grid>
              {!_.isEmpty(projectOptions) && (
                <Grid item xs={12} md={6} lg={2}>
                  <CustomMultiSelect
                    value={values.projectIds}
                    options={projectOptions}
                    fieldName="projectIds"
                    handleChange={handleChange}
                    label="Select project:"
                    disabled={!hasLoaded}
                  />
                </Grid>
              )}
              {!_.isEmpty(squadOptions) && (
                <Grid item xs={12} md={6} lg={2}>
                  <CustomMultiSelect
                    value={values.squadIds}
                    options={squadOptions}
                    fieldName="squadIds"
                    handleChange={handleChange}
                    label="Select squad:"
                    disabled={!hasLoaded}
                  />
                </Grid>
              )}
            </Grid>
            <Grid container justify="flex-end">
              <Box mt={3}>
                <Button
                  size="small"
                  disabled={isSubmitting || !hasLoaded}
                  type="submit"
                  variant="contained"
                  color="primary"
                  startIcon={<FilterList />}
                >
                  {isSubmitting ? 'Filtering..' : 'Filter'}
                </Button>
              </Box>
              <Box mt={3} ml={2}>
                <Button
                  size="small"
                  onClick={() => onClear(resetForm)}
                  variant="contained"
                  startIcon={<Clear />}
                  disabled={!hasLoaded}
                >
                  Clear filters
                </Button>
              </Box>
            </Grid>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default CustomFilter;

CustomFilter.propTypes = {
  onClear: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  shouldDisplayProjectFilter: PropTypes.bool,
  shouldDisplaySquadFilter: PropTypes.bool,
  title: PropTypes.string,
  hasLoaded: PropTypes.bool,
  squadList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    })
  ),
  projectList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      lastDeployed: PropTypes.string,
      notes: PropTypes.string,
      squadId: PropTypes.number,
      squadName: PropTypes.string,
      status: PropTypes.string,
      title: PropTypes.string,
    })
  ),
};

CustomFilter.defaultProps = {
  shouldDisplayProjectFilter: false,
  shouldDisplaySquadFilter: false,
  title: '',
  hasLoaded: false,
  squadList: [],
  projectList: [],
};
