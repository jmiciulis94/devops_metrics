import {
  Container,
  Grid,
  Button,
  ButtonGroup,
  Box,
  RadioGroup,
  Radio,
  FormControlLabel,
  CircularProgress,
  Typography,
  Paper,
} from '@material-ui/core';
import React, {useState, useEffect} from 'react';
import {
  Chart,
  ArgumentAxis,
  Legend,
  ValueAxis,
  Title,
  Tooltip,
  LineSeries,
} from '@devexpress/dx-react-chart-material-ui';
import PropTypes from 'prop-types';
import {ArgumentScale, EventTracker} from '@devexpress/dx-react-chart';
import {scaleBand} from '@devexpress/dx-chart-core';
import useProgressStyles from './useStyles';
import TooltipSheet from '../TooltipSheet';
import TooltipArrow from '../TooltipArrow';
import {useFetch} from '../../useFetch';
import getRequestUrl from '../../utils/getRequestUrl';

const ProgressChart = ({
  projectId,
  onLoad,
  hasLoaded,
  filters: {dateFrom, dateTo, squadIds, projectIds},
  handleRefresh,
}) => {
  const classes = useProgressStyles();

  /* eslint-disable react/jsx-props-no-spreading */
  const LegendRoot = props => {
    return <Legend.Root {...props} className={classes.legendRoot} />;
  };
  const Header = props => {
    return <Box {...props} className={classes.header} />;
  };
  /* eslint-enable */

  const [data, setData] = useState([]);
  const [chart, setChart] = useState('deployment-frequency');
  const [period, setPeriod] = useState('day');
  const [title, setTitle] = useState(`Deployment frequency, deployments/${period}`);
  const [warningMessage, setWarningMessage] = useState('');
  const globalUrl = `/global/progress/${chart}?period=${period}`;
  const singleProjectUrl = `/projects/${projectId}/progress/${chart}?period=${period}`;

  const handleLoad = didLoad => {
    onLoad(didLoad);
  };

  const takePartOfDate = date => {
    if (period === 'month') return date.slice(0, 7);
    if (period === 'year') return date.slice(0, 4);
    if (period === 'day') return date.slice(5, 10);
    return date;
  };

  const dateCountForPeriod = {
    day: 30,
    month: 365,
    year: 365 * 5,
  };

  const calculateIfDateIntervalTooLarge = () => {
    const dateCompareTo = Date.parse(dateTo);
    const dateCompareFrom = Date.parse(dateFrom);
    const dateDifference = Math.floor(
      ((dateCompareTo || Date.now()) - (dateCompareFrom || Date.UTC(1970, 1, 1))) /
        (1000 * 60 * 60 * 24)
    );
    const message = 'The progress chart currently only shows the data of the last';
    const adviseMessage = 'You could either select a smaller date interval or change the period.';
    if (period === 'day' && dateDifference > dateCountForPeriod.day)
      setWarningMessage(`${message} 30 days. ${adviseMessage}`);
    else if (period === 'month' && dateDifference > dateCountForPeriod.month)
      setWarningMessage(`${message} year. ${adviseMessage}`);
    else if (period === 'year' && dateDifference > dateCountForPeriod.year)
      setWarningMessage(`${message} 5 years. ${adviseMessage}`);
    else setWarningMessage('');
  };

  const fetchData = async url => {
    const response = await useFetch(url, 'GET');
    const jsonData = await response.json();
    const dataToSet = jsonData.map(record => {
      return {
        ...record,
        dateTooltip: record.date,
        date: takePartOfDate(record.date),
        median: record.median ? record.median : 0,
      };
    });
    setData(dataToSet);
    handleLoad(true);
  };

  useEffect(() => {
    handleLoad(false);
    if (projectId) {
      const url = getRequestUrl(singleProjectUrl, {dateFrom, dateTo});
      fetchData(url);
    } else {
      const url = getRequestUrl(globalUrl, {dateFrom, dateTo, squadIds, projectIds});
      fetchData(url);
    }
    calculateIfDateIntervalTooLarge();
  }, [chart, period, projectId, squadIds, projectIds, dateTo, dateFrom, handleRefresh]);

  const Content = ({targetItem, text}) => {
    const whichDate = targetItem.point;
    const number = Number.parseFloat(text).toFixed(2);
    Content.propTypes = {
      targetItem: PropTypes.shape({
        point: PropTypes.number,
      }),
      text: PropTypes.string,
    };

    Content.defaultProps = {
      targetItem: null,
      text: '',
    };
    return (
      <>
        {period === 'day' ? (
          <Tooltip.Content text={data[whichDate].dateTooltip} className={classes.tooltipDate} />
        ) : null}
        <Tooltip.Content text={number} className={classes.tooltipValue} />
      </>
    );
  };
  return (
    <>
      <Container>
        <Paper square>
          <Grid container spacing={0}>
            <Grid item xs={12}>
              <Header>
                <ButtonGroup variant="text" fullWidth>
                  <Button
                    onClick={() => {
                      setChart('deployment-frequency');
                      setTitle(`Deployment frequency, deployments/${period}`);
                    }}
                  >
                    Deployment frequency
                  </Button>
                  <Button
                    onClick={() => {
                      setChart('lead-time');
                      setTitle('Lead time, minutes');
                    }}
                  >
                    Lead time
                  </Button>
                  <Button
                    onClick={() => {
                      setChart('deployment-duration');
                      setTitle('Deployment duration, minutes');
                    }}
                  >
                    Deployment duration
                  </Button>
                  <Button
                    onClick={() => {
                      setChart('failure-rate');
                      setTitle('Failure rate, %');
                    }}
                  >
                    Failure rate
                  </Button>
                </ButtonGroup>
              </Header>
            </Grid>
            {
              // eslint-disable-next-line no-nested-ternary
              hasLoaded ? (
                data.length > 2 ? (
                  <Grid item xs={12} className={classes.padding}>
                    <Chart data={data}>
                      <ArgumentAxis showGrid />
                      <ArgumentScale factory={scaleBand} />
                      <Legend rootComponent={LegendRoot} position="bottom" />
                      <Title text={title} />
                      <EventTracker />
                      <Tooltip
                        sheetComponent={TooltipSheet}
                        arrowComponent={TooltipArrow}
                        contentComponent={Content}
                      />
                      <ValueAxis />
                      <LineSeries
                        name="average"
                        valueField="average"
                        argumentField="date"
                        color="#746595"
                      />
                      <LineSeries
                        name="median"
                        valueField="median"
                        argumentField="date"
                        color="#d5c3f9"
                      />
                    </Chart>
                  </Grid>
                ) : (
                  <Grid container item xs={12} alignItems="center" justify="center">
                    <Chart data={[]}>
                      <Typography variant="h5">
                        The date interval is too small to display data for this period
                      </Typography>
                    </Chart>
                  </Grid>
                )
              ) : (
                <Grid container item xs={12} alignItems="center" justify="center">
                  <Chart data={[]}>
                    <CircularProgress />
                  </Chart>
                </Grid>
              )
            }
            <Grid item xs={12} className={classes.padding}>
              <RadioGroup
                row
                defaultValue="month"
                value={period}
                onChange={e => {
                  setPeriod(e.target.value);
                }}
              >
                <FormControlLabel value="day" control={<Radio color="primary" />} label="Day" />
                <FormControlLabel value="month" control={<Radio color="primary" />} label="Month" />
                <FormControlLabel value="year" control={<Radio color="primary" />} label="Year" />
              </RadioGroup>
              <Typography color="primary" variant="body2">
                {warningMessage}
              </Typography>
            </Grid>
          </Grid>
        </Paper>
      </Container>
    </>
  );
};

ProgressChart.propTypes = {
  projectId: PropTypes.number,
  filters: PropTypes.shape({
    dateFrom: PropTypes.string,
    dateTo: PropTypes.string,
    squadIds: PropTypes.arrayOf(PropTypes.number),
    projectIds: PropTypes.arrayOf(PropTypes.number),
  }),
  onLoad: PropTypes.func.isRequired,
  hasLoaded: PropTypes.bool,
  handleRefresh: PropTypes.bool,
};

ProgressChart.defaultProps = {
  projectId: null,
  filters: {
    dateFrom: '',
    dateTo: '',
    squadIds: [],
    projectIds: [],
  },
  hasLoaded: false,
  handleRefresh: false,
};

export default ProgressChart;
