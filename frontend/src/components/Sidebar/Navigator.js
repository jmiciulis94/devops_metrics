import React from 'react';
import List from '@material-ui/core/List';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import PeopleIcon from '@material-ui/icons/People';
import SettingsIcon from '@material-ui/icons/Settings';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import WorkIcon from '@material-ui/icons/Work';
import CropSquareIcon from '@material-ui/icons/CropSquare';
import HelpIcon from '@material-ui/icons/Help';
import {Link} from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {AccountBox} from '@material-ui/icons';
import {useNavigatorStyles} from './useStyles';
import './Sidebar.css';

const Navigator = () => {
  const classes = useNavigatorStyles();

  return (
    <List className={classes.root} id="scrollbar">
      <ListItem button to="/projects" component={Link}>
        <ListItemIcon>
          <WorkIcon />
        </ListItemIcon>
        <ListItemText primary="Projects" />
      </ListItem>

      <ListItem button to="/metrics" component={Link}>
        <ListItemIcon>
          <EqualizerIcon />
        </ListItemIcon>
        <ListItemText primary="Metrics" />
      </ListItem>

      <ListItem button to="/progress" component={Link}>
        <ListItemIcon>
          <TrendingUpIcon />
        </ListItemIcon>
        <ListItemText primary="Progress" />
      </ListItem>

      <ListItem button to="/quadrant" component={Link}>
        <ListItemIcon>
          <CropSquareIcon />
        </ListItemIcon>
        <ListItemText primary="Quadrant" />
      </ListItem>

      <ListItem button to="/squads" component={Link}>
        <ListItemIcon>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText primary="Squads" />
      </ListItem>

      {sessionStorage.getItem('role') === 'ADMIN' ? (
        <>
          <ListItem button to="/users" component={Link}>
            <ListItemIcon>
              <AccountBox />
            </ListItemIcon>
            <ListItemText primary="Users" />
          </ListItem>
          <ListItem button to="/settings" component={Link}>
            <ListItemIcon>
              <SettingsIcon />
            </ListItemIcon>
            <ListItemText primary="Settings" />
          </ListItem>
        </>
      ) : null}

      <ListItem button to="/help" component={Link}>
        <ListItemIcon>
          <HelpIcon />
        </ListItemIcon>
        <ListItemText primary="Help" />
      </ListItem>
    </List>
  );
};

export default Navigator;
