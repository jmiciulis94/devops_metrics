import makeStyles from '@material-ui/core/styles/makeStyles';

export const useNavigatorStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: 300,
    top: 100,
    bottom: 160,
    position: 'fixed',
    overflowY: 'scroll',
    scrollbarWidth: 'none',
  },
}));

export const useUserSectionStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: 300,
    height: 140,
    backgroundColor: '#aeaeae',
    bottom: 0,
    position: 'fixed',
    paddingTop: 20,
    alignItems: 'center',
  },
  logout: {
    width: 30,
    height: 30,
    bottom: -10,
  },
  userIcon: {
    width: 60,
    height: 60,
    paddingLeft: 10,
  },
  name: {
    fontSize: 20,
    fontWeight: 700,
  },
}));

export const useSidebarStyles = makeStyles(() => ({
  typography: {
    fontSize: '1.4rem',
    textAlign: 'left',
    fontWeight: 'bold',
    width: 180,
  },
  logo: {
    width: 100,
    margin: '0 auto',
  },
}));
