import React from 'react';
import {Typography} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Navigator from './Navigator';
import UserSection from './UserSection';
import {useSidebarStyles} from './useStyles';
import logo from './logo.png';

const Sidebar = () => {
  const classes = useSidebarStyles();
  return (
    <Grid container direction="column">
      <Grid item xs={2} style={{position: 'fixed'}}>
        <table>
          <thead>
            <tr>
              <td>
                <img src={logo} alt="logo" className={classes.logo} />
              </td>
              <td>
                <Typography className={classes.typography}>DevOps Metrics</Typography>
              </td>
            </tr>
          </thead>
        </table>
      </Grid>
      <Grid item xs={7}>
        <Navigator />
      </Grid>
      <Grid item xs={3}>
        <UserSection />
      </Grid>
    </Grid>
  );
};

export default Sidebar;
