import React from 'react';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import IconButton from '@material-ui/core/IconButton';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import {Tooltip, Typography} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import {useUserSectionStyles} from './useStyles';
import {setToken} from '../../state/actions/token';
import store from '../../state/store';

const UserSection = () => {
  const classes = useUserSectionStyles();

  const firstName = sessionStorage.getItem('firstName');
  const lastName = sessionStorage.getItem('lastName');
  const email = sessionStorage.getItem('email');
  const fullName = `${firstName} ${lastName}`;

  function handleClick() {
    sessionStorage.clear();
    store.dispatch(setToken(null));
  }

  return (
    <div className={classes.root}>
      <Grid container direction="row" alignItems="center">
        <Grid item xs={3}>
          <AccountCircleIcon className={classes.userIcon} />
        </Grid>
        <Grid item xs={9}>
          <Typography className={classes.name}>{fullName}</Typography>
          <Typography>{email}</Typography>
        </Grid>
      </Grid>

      <Tooltip title="Logout" placement="left">
        <IconButton onClick={handleClick} className={classes.logout}>
          <ExitToAppIcon className={classes.logout} />
        </IconButton>
      </Tooltip>
    </div>
  );
};

export default UserSection;
