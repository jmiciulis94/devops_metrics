import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useHistory} from 'react-router';
import Routes from './Routes';
import Header from './components/Header';
import Sidebar from './components/Sidebar';
import {setToken} from './state/actions/token';

const Layout = () => {
  const token = useSelector(state => state.token);
  const dispatch = useDispatch();
  const history = useHistory();
  useEffect(() => {
    if (token === null) {
      const storedToken = sessionStorage.getItem('token');
      if (storedToken) {
        dispatch(setToken(storedToken));
      } else {
        history.push('/login');
      }
    }
  }, [token]);
  return (
    <div className="App">
      <header>
        <Header />
      </header>

      <nav>
        <Sidebar />
      </nav>

      <main>
        <Routes />
      </main>
    </div>
  );
};

export default Layout;
