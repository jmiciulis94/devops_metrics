import _ from 'lodash';
import {setToken} from './state/actions/token';
import store from './state/store';
import {API_URL} from './config/api';

/* eslint-disable import/prefer-default-export */

const refreshToken = () => {
  return fetch(`${API_URL}auth/refresh`, {
    method: 'POST',
    headers: {
      Refresh: store.getState(store).token || sessionStorage.getItem('token'),
    },
  }).then(response => {
    if (response.status !== 200) {
      return false;
    }

    const newToken = response.headers.get('Authorization');
    sessionStorage.setItem('token', newToken);
    store.dispatch(setToken(newToken));

    return true;
  });
};

export const useFetch = (url, method, body = {}, headers = {}) => {
  const params = {
    method,
    headers: {
      ...headers,
      'content-type': 'application/json',
      Authorization: store.getState().token || sessionStorage.getItem('token'),
    },
  };

  if (!_.isEmpty(body)) {
    params.body = JSON.stringify(body);
  }

  const urlWithContext = `${API_URL}${url}`;

  /* eslint-disable */
  return fetch(urlWithContext, params).then(response => {
    if (response.status === 401 && response.headers.get('X-Needs-Refresh') === 'true') {
      return refreshToken().then(shouldRetryFetch => {
        if (shouldRetryFetch) {
          return fetch(url, params);
        } else {
          sessionStorage.clear();
          store.dispatch(setToken(null));
        }
      });
    } else if (response.status === 401) {
      sessionStorage.clear();
      store.dispatch(setToken(null));

      return response;
    }

    return response;
  });
};
