CREATE TABLE devops_metrics.settings (
    id BIGSERIAL NOT NULL,
    failure_rate_limit NUMERIC(3, 2),
    deployment_frequency_limit NUMERIC(9, 2),
    PRIMARY KEY (id)
);