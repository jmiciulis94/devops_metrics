-- DROP SCHEMA IF EXISTS devops_metrics CASCADE;

CREATE SCHEMA IF NOT EXISTS devops_metrics;

CREATE TABLE devops_metrics.user (
	id BIGSERIAL NOT NULL,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	deleted BOOLEAN NOT NULL DEFAULT FALSE,
	PRIMARY KEY (id)
);

CREATE TABLE devops_metrics.squad (
	id BIGSERIAL NOT NULL,
	name VARCHAR(255) NOT NULL,
	deleted BOOLEAN NOT NULL DEFAULT FALSE,
	PRIMARY KEY (id)
);

CREATE TABLE devops_metrics.project (
	id BIGSERIAL NOT NULL,
	title VARCHAR(255) NOT NULL,
	notes TEXT,
	status VARCHAR(30) NOT NULL DEFAULT 'PRE_PRODUCTION' CHECK (status IN ('PRE_PRODUCTION', 'IN_PRODUCTION')),
	deleted BOOLEAN NOT NULL DEFAULT FALSE,
	squad_id BIGINT REFERENCES devops_metrics.squad(id),
	PRIMARY KEY (id)
);

CREATE TABLE devops_metrics.deployment (
	id BIGSERIAL NOT NULL,
	date TIMESTAMP NOT NULL,
	is_defect BOOLEAN NOT NULL,
	notes TEXT,
	deleted BOOLEAN NOT NULL DEFAULT FALSE,
	project_id BIGINT REFERENCES devops_metrics.project(id),
	PRIMARY KEY (id)
);

CREATE TABLE devops_metrics.metric (
	id BIGSERIAL NOT NULL,
	deployment_duration INTEGER NOT NULL,
	lead_time INTEGER NOT NULL,
	date TIMESTAMP NOT NULL,
	project_id BIGINT REFERENCES devops_metrics.project(id),
	PRIMARY KEY (id)
);
