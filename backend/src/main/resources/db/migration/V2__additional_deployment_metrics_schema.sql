ALTER TABLE devops_metrics.deployment
    ADD COLUMN deployment_duration bigint DEFAULT NULL;

ALTER TABLE devops_metrics.deployment
    ADD COLUMN lead_time bigint DEFAULT NULL;