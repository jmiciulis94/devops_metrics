ALTER TABLE devops_metrics.settings
    ALTER COLUMN failure_rate_limit TYPE NUMERIC(5, 4);