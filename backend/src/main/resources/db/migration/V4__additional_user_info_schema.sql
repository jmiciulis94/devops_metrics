ALTER TABLE devops_metrics.user
    ADD COLUMN role VARCHAR(30) NOT NULL DEFAULT 'USER' CHECK (role IN ('USER', 'ADMIN'));