package com.devbridge.internal.devopsmetrics.service;

import com.devbridge.internal.devopsmetrics.converter.DeploymentConverter;
import com.devbridge.internal.devopsmetrics.dto.DeploymentDto;
import com.devbridge.internal.devopsmetrics.model.Deployment;
import com.devbridge.internal.devopsmetrics.repository.DeploymentRepository;
import com.devbridge.internal.devopsmetrics.util.FilteringUtil;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import com.devbridge.internal.devopsmetrics.exception.NotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DeploymentService {
    private final DeploymentRepository deploymentRepository;
    private final DeploymentConverter deploymentConverter;

    public DeploymentService(DeploymentRepository deploymentRepository, DeploymentConverter deploymentConverter) {
        this.deploymentRepository = deploymentRepository;
        this.deploymentConverter = deploymentConverter;
    }

    public Deployment saveDeployment(DeploymentDto deploymentDto){
        Deployment deployment = deploymentConverter.dtoToEntity(deploymentDto);
        LocalDateTime dt = LocalDateTime.parse(deploymentDto.getDate(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        deployment.setDate(dt);
        deploymentRepository.save(deployment);
        return deployment;
    }
    
    public DeploymentDto updateDeployment(DeploymentDto deploymentDto) {
        Deployment deployment = deploymentRepository.findByIdAndDeletedFalse(deploymentDto.getId());
        LocalDateTime dt = LocalDateTime.parse(deploymentDto.getDate(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        deployment.setDate(dt);
        deployment.setIsDefect(deploymentDto.getIsDefect());
        deployment.setNotes(deploymentDto.getNotes());
        deployment.setDeploymentDuration(deploymentDto.getDeploymentDuration());
        deployment.setLeadTime(deploymentDto.getLeadTime());
        deploymentRepository.save(deployment);
        return deploymentDto;
    }

    public boolean hasDeployments(Long projectId) {
        return !deploymentRepository.findAllByProjectIdAndDeletedFalse(projectId).isEmpty();
    }

    public DeploymentDto getDeployment(Long id) {
        Deployment deployment = deploymentRepository.findByIdAndDeletedFalse(id);
        if (deployment == null) throw new NotFoundException(String.format("Deployment with id=%d not found", id), "Deployment");
        ModelMapper modelMapper = new ModelMapper();
        DeploymentDto deploymentDto = modelMapper.map(deployment, DeploymentDto.class);
        return deploymentDto;
    }

    public Deployment deleteDeployment(Long id) {
        Deployment deployment = deploymentRepository.findByIdAndDeletedFalse(id);
        if (deployment == null) {
            throw new NotFoundException(String.format("Deployment with id=%d not found", id), "Deployment");
        } else {
            deployment.setDeleted(true);
            deploymentRepository.save(deployment);
            return deployment;
        }
    }

    public void deleteDeployments(Long projectId) {
        List<Deployment> deployments = deploymentRepository.findAllByProjectIdAndDeletedFalse(projectId);
        for (Deployment deployment : deployments) {
            deployment.setDeleted(true);
        }
        deploymentRepository.saveAll(deployments);
    }

    public List<DeploymentDto> getDeployments(Long projectId, int limit, int page, String dateFrom, String dateTo, HttpHeaders headers) {
        dateFrom = FilteringUtil.checkDate(dateFrom);
        dateTo = FilteringUtil.checkDate(dateTo);

        String count = String.format("%d", deploymentRepository.findFilteredCount(projectId, dateFrom, dateTo));
        headers.add("X-Total-Count", count);
        headers.add("Access-Control-Expose-Headers", "*");
        Pageable pageable = PageRequest.of(page, limit);
        List<Deployment> deployments = deploymentRepository.findFiltered(pageable,projectId, dateFrom, dateTo);
        return deployments.stream().map(deploymentConverter::convertEntityToViewDto).collect(Collectors.toList());
    }
}
