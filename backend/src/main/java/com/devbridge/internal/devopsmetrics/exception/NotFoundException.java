package com.devbridge.internal.devopsmetrics.exception;

public class NotFoundException extends RuntimeException {
    private final String field;
    public NotFoundException(String message, String field) {
        super(message);
        this.field = field;
    }

    public String getField() {
        return field;
    }
}
