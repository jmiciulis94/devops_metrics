package com.devbridge.internal.devopsmetrics.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GlobalMetricsStatDto {
    private String name;
    private Float leadTime;
    private Float deploymentDuration;
    private Float failureRate;
    private Float deploymentFrequency;
}
