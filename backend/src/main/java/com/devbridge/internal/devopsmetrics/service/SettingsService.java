package com.devbridge.internal.devopsmetrics.service;

import com.devbridge.internal.devopsmetrics.converter.SettingsConverter;
import com.devbridge.internal.devopsmetrics.dto.SettingsDto;
import com.devbridge.internal.devopsmetrics.exception.NotFoundException;
import com.devbridge.internal.devopsmetrics.model.Settings;
import com.devbridge.internal.devopsmetrics.repository.SettingsRepository;
import java.math.BigDecimal;
import java.math.RoundingMode;
import org.springframework.stereotype.Service;

@Service
public class SettingsService {

    private final SettingsRepository settingsRepository;
    private final SettingsConverter settingsConverter;

    public SettingsService(SettingsRepository settingsRepository, SettingsConverter settingsConverter) {
        this.settingsRepository = settingsRepository;
        this.settingsConverter = settingsConverter;
    }

    public SettingsDto updateSettings(SettingsDto settingsDto) {
        Settings settings = settingsRepository.findFirstBy();
        BigDecimal HUNDRED = new BigDecimal(100);
        if (settings == null) {
            settings = new Settings();
        }
        settings.setDeploymentFrequencyLimit(settingsDto.getDeploymentFrequencyLimit());
        settings.setFailureRateLimit(settingsDto.getFailureRateLimit().divide(HUNDRED, 4, RoundingMode.HALF_UP));
        settingsRepository.save(settings);
        return settingsDto;
    }

    public SettingsDto getSettings() {
        Settings settings = settingsRepository.findFirstBy();
        if (settings == null) throw new NotFoundException("Settings were not found", "Settings");
        return settingsConverter.convertEntityToDto(settings);
    }
}
