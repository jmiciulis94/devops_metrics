package com.devbridge.internal.devopsmetrics.model;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Data
@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "project", schema="devops_metrics")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;

    private String notes;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private ProjectStatus status;

    @Column(nullable = false)
    private Boolean deleted = false;

    @JoinColumn(name = "squad_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private Squad squad;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "project")
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonManagedReference
    private List<Deployment> deployments = new ArrayList<>();

}
