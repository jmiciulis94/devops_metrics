package com.devbridge.internal.devopsmetrics.exception;

public class WrongCredentialsException extends RuntimeException {
    private final String field;

    public WrongCredentialsException(String message, String field) {
        super(message);
        this.field = field;
    }

    public String getField() {
        return field;
    }
}
