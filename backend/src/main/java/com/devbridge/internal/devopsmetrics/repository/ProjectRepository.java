package com.devbridge.internal.devopsmetrics.repository;

import com.devbridge.internal.devopsmetrics.model.Project;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {
    List<Project> findAllByDeletedFalse();
    List<Project> findAllByDeletedFalseOrderByTitle();
    List<Project> findAllByDeletedFalseOrderByTitleAscSquadAsc(Pageable pageable);
    Project findByIdAndDeletedFalse(Long id);
    List<Project> findAllBySquadIdAndDeletedFalse(Long id);
    Boolean existsByTitleAndDeletedFalse(String title);

    @Query(value = "select count(*)" +
            "from devops_metrics.project\n" +
            "left join (select max(deployment.date) as last_deployment, project_id\n" +
            "from devops_metrics.deployment\n" +
            "where deployment.deleted = false\n" +
            "group by deployment.project_id) as tmp on project.id = tmp.project_id\n" +
            "where project.deleted = false\n" +
            "and case when :dateFrom!='none' then last_deployment >= cast(:dateFrom as timestamp) else last_deployment<> all('{}') end\n" +
            "and case when :dateTo!='none' then last_deployment <= cast(:dateTo as timestamp) else last_deployment<> all('{}') end \n" +
            "and coalesce(project.id in (:projectIds), project.id <> all('{}')) \n"+
            "and coalesce(project.squad_id in (:squadIds), project.squad_id <> all('{}'))\n"
            ,nativeQuery = true)
    int findFilteredCount(@Param("dateFrom") String dateFrom,
                               @Param("dateTo") String dateTo,
                               @Param("squadIds")List<Integer> squadIds,
                               @Param("projectIds") List<Integer> projectIds
    );


    @Query(value = "select project.id, project.title, project.notes, project.status, project.deleted, project.squad_id\n" +
            "from devops_metrics.project\n" +
            "left join (select max(deployment.date) as last_deployment, project_id\n" +
            "from devops_metrics.deployment\n" +
            "where deployment.deleted = false\n" +
            "group by deployment.project_id) as tmp on project.id = tmp.project_id\n" +
            "where project.deleted = false\n" +
            "and case when :dateFrom!='none' then last_deployment >= cast(:dateFrom as timestamp) else last_deployment<> all('{}') end\n" +
            "and case when :dateTo!='none' then last_deployment <= cast(:dateTo as timestamp) else last_deployment<> all('{}') end \n" +
            "and coalesce(project.id in (:projectIds), project.id <> all('{}')) \n"+
            "and coalesce(project.squad_id in (:squadIds), project.squad_id <> all('{}'))\n"+
            "group by project.id, tmp.last_deployment, tmp.project_id\n" +
            "order by project.id,last_deployment ASC", nativeQuery = true)
    List<Project> findFiltered(Pageable pageable,
                               @Param("dateFrom") String dateFrom,
                               @Param("dateTo") String dateTo,
                               @Param("squadIds")List<Integer> squadIds,
                               @Param("projectIds") List<Integer> projectIds
    );
}
