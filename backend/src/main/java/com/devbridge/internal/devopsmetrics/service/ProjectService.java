package com.devbridge.internal.devopsmetrics.service;

import com.devbridge.internal.devopsmetrics.converter.ProjectConverter;
import com.devbridge.internal.devopsmetrics.dto.ProjectDto;
import com.devbridge.internal.devopsmetrics.dto.ProjectFormDto;
import com.devbridge.internal.devopsmetrics.exception.NotFoundException;
import com.devbridge.internal.devopsmetrics.exception.RecordAlreadyExistsException;
import com.devbridge.internal.devopsmetrics.model.Project;
import com.devbridge.internal.devopsmetrics.model.ProjectStatus;
import com.devbridge.internal.devopsmetrics.repository.ProjectRepository;

import java.util.List;
import java.util.stream.Collectors;

import com.devbridge.internal.devopsmetrics.repository.SquadRepository;
import com.devbridge.internal.devopsmetrics.util.FilteringUtil;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {

    private final ProjectRepository projectRepository;
    private final ProjectConverter projectConverter;
    private final SquadRepository squadRepository;

    public ProjectService(ProjectRepository projectRepository, ProjectConverter projectConverter, SquadRepository squadRepository) {
        this.projectRepository = projectRepository;
        this.projectConverter = projectConverter;
        this.squadRepository = squadRepository;
    }

    public Project saveProject (ProjectFormDto projectFormDto){
        Project project = projectConverter.convertFormDtoToEntity(projectFormDto);
        checkIfExists(project.getTitle(), "create");
        project.setStatus(ProjectStatus.PRE_PRODUCTION);
        projectRepository.save(project);
        return project;
    }

    public void setProjectStatusInProduction(Long id) {
        Project project = projectRepository.findByIdAndDeletedFalse(id);
        if (project.getStatus() != ProjectStatus.IN_PRODUCTION) {
            project.setStatus(ProjectStatus.IN_PRODUCTION);
            projectRepository.save(project);
        }
    }

    public void setProjectStatusPreProduction(Long id) {
        Project project = projectRepository.findByIdAndDeletedFalse(id);
        if (project.getStatus() != ProjectStatus.PRE_PRODUCTION) {
            project.setStatus(ProjectStatus.PRE_PRODUCTION);
            projectRepository.save(project);
        }
    }

    public ProjectFormDto updateProject(ProjectFormDto projectFormDto) {
        Project project = projectRepository.findByIdAndDeletedFalse(projectFormDto.getId());
        if (!projectFormDto.getTitle().equals(project.getTitle()))
            checkIfExists(projectFormDto.getTitle(), "update");
        project.setTitle(projectFormDto.getTitle());
        project.setSquad(squadRepository.findByIdAndDeletedFalse(projectFormDto.getSquadId()));
        project.setNotes(projectFormDto.getNotes());
        projectRepository.save(project);
        return projectConverter.convertEntityToFormDto(project);
    }

    public List<ProjectDto> getProjects() {
        List<Project> projectList = projectRepository.findAllByDeletedFalseOrderByTitle();
        return projectList.stream().map(projectConverter::convertEntityToDto).collect(Collectors.toList());
    }

    public List<ProjectDto> getProjects(Integer limit, Integer page, HttpHeaders headers) {
        String count = String.format("%d", projectRepository.findAllByDeletedFalse().size());
        headers.add("X-Total-Count", count);
        headers.add("Access-Control-Expose-Headers", "*");
        Pageable pageable = PageRequest.of(page, limit);
        List<Project> projectList = projectRepository.findAllByDeletedFalseOrderByTitleAscSquadAsc(pageable);
        return projectList.stream().map(projectConverter::convertEntityToDto).collect(Collectors.toList());
    }

    public ProjectFormDto getProject(Long id) {
        Project project = projectRepository.findByIdAndDeletedFalse(id);
        if (project == null) {
            throw new NotFoundException(String.format("Project with id=%d not found", id), "Project");
        } else return projectConverter.convertEntityToFormDto(project);
    }

    public Project deleteProject(Long id) {
        Project project = projectRepository.findByIdAndDeletedFalse(id);
        if (project == null) {
            return null;
        } else {
            project.setDeleted(true);
            projectRepository.save(project);
            return project;
        }
    }

    private void checkIfExists(String name, String action) {
        boolean doesExist = projectRepository.existsByTitleAndDeletedFalse(name);
        String message = String.format("Cannot %s project, this project already exists.", action);
        if (doesExist) {
            throw new RecordAlreadyExistsException(message, "Project");
        }
    }

    public List<ProjectDto> getProjects(Integer limit, Integer page, String dateFrom, String dateTo,
                                        List<String> squadIds, List<String> projectIds, HttpHeaders headers) {
        List<Integer> squads = FilteringUtil.convertStringArrayToIntArray(squadIds);
        List<Integer> projects = FilteringUtil.convertStringArrayToIntArray(projectIds);

        dateFrom = FilteringUtil.checkDate(dateFrom);
        dateTo = FilteringUtil.checkDate(dateTo);

        String count = String.format("%d", projectRepository.findFilteredCount(dateFrom, dateTo, squads, projects));
        headers.add("X-Total-Count", count);
        headers.add("Access-Control-Expose-Headers", "*");
        Pageable pageable = PageRequest.of(page, limit);
        List<Project> projectList = projectRepository.findFiltered(pageable, dateFrom, dateTo, squads, projects);
        return projectList.stream().map(projectConverter::convertEntityToDto).collect(Collectors.toList());
    }
}
