package com.devbridge.internal.devopsmetrics.exception;

public class CannotDeleteException extends RuntimeException {
    private final String field;

    public CannotDeleteException(String message, String field) {
        super(message);
        this.field = field;
    }

    public String getField() {
        return field;
    }
}
