package com.devbridge.internal.devopsmetrics.util;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FilteringUtil {
    public static MapSqlParameterSource mapSqlParameters(String period, String dateFrom, String dateTo, Long id){
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("dateFrom", dateFrom);
        parameters.addValue("dateTo", dateTo);
        parameters.addValue("projectId", id);
        parameters.addValue("period", period);
        return parameters;
    }
    public static MapSqlParameterSource mapSqlParameters(String period, String dateFrom, String dateTo, List<Integer> squadIds, List<Integer> projectIds){
        MapSqlParameterSource parameters = mapSqlParameters(dateFrom, dateTo, squadIds, projectIds);
        parameters.addValue("period", period);
        return parameters;
    }

    public static MapSqlParameterSource mapSqlParameters(String dateFrom, String dateTo, List<Integer> squadIds, List<Integer> projectIds) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("dateFrom", dateFrom);
        parameters.addValue("dateTo", dateTo);
        if (!squadIds.isEmpty()) parameters.addValue("squadIds", squadIds);
        else  parameters.addValue("squadIds", null);
        if (!projectIds.isEmpty()) parameters.addValue("projectIds", projectIds);
        else  parameters.addValue("projectIds", null);
        return parameters;
    }

    public static List<Integer> convertStringArrayToIntArray(List<String> stringArray){
        List<Integer> array = new ArrayList<>();
        if (stringArray != null){
            for(String number : stringArray) {
                array.add(Integer.parseInt(number));
            }
        }
        return array;
    }

    public static String checkDate(String date){
        try {
            LocalDateTime.parse(date);
        }
        catch (Exception e) {
            date = "none";
        }
        return date;
    }

}
