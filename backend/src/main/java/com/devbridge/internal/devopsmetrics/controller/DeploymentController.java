package com.devbridge.internal.devopsmetrics.controller;

import com.auth0.jwt.JWT;
import com.devbridge.internal.devopsmetrics.dto.DeploymentDto;
import com.devbridge.internal.devopsmetrics.exception.BadRequestException;
import com.devbridge.internal.devopsmetrics.model.Deployment;
import com.devbridge.internal.devopsmetrics.service.DeploymentService;
import com.devbridge.internal.devopsmetrics.service.ProjectService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.devbridge.internal.devopsmetrics.util.SecurityConstants.TOKEN_PREFIX;


@RestController
@RequestMapping(value = "/projects/{projectId}/deployments")
public class DeploymentController {
    private final DeploymentService deploymentService;
    private final ProjectService projectService;

    public DeploymentController(DeploymentService deploymentService, ProjectService projectService) {
        this.deploymentService = deploymentService;
        this.projectService = projectService;
    }

    @PostMapping
    public ResponseEntity<Deployment> saveDeployment(@RequestBody @Valid DeploymentDto deploymentDto, @PathVariable("projectId") Long projectId) {
        deploymentDto.setProjectId(projectId);
        Deployment deployment = deploymentService.saveDeployment(deploymentDto);
        projectService.setProjectStatusInProduction(projectId);
        return ResponseEntity.ok(deployment);
    }

    @PostMapping("/auto")
    public ResponseEntity<?> saveDeployment(@RequestBody @Valid DeploymentDto deploymentDto,
                                            @RequestHeader("Authorization") String token,
                                            @PathVariable("projectId") Long projectId) {
        try {
            String project = JWT.decode(token.replace(TOKEN_PREFIX, "")).getSubject();
            if (project.equals(projectId.toString())) {
                deploymentDto.setProjectId(projectId);
                Deployment deployment = deploymentService.saveDeployment(deploymentDto);
                Long id = deployment.getProject().getId();
                projectService.setProjectStatusInProduction(id);
                return ResponseEntity.ok(deployment);
            } else {
                return new ResponseEntity<>(HttpStatus.valueOf(403));
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.valueOf(400));
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<DeploymentDto> updateDeployment(@PathVariable(value = "id") Long deploymentId,
                                                          @Valid @RequestBody DeploymentDto deploymentDto) {
        if (deploymentDto.getId().equals(deploymentId)) {
            return ResponseEntity.ok(deploymentService.updateDeployment(deploymentDto));
        } else {
            throw new BadRequestException("Invalid request url.", "Deployment");
        }
    }

    @GetMapping
    public ResponseEntity<List<DeploymentDto>> getDeployments(@PathVariable("projectId") Long projectId,
                                                              @RequestParam("limit") int limit, @RequestParam("page") int page,
                                                              @RequestParam(value = "dateFrom", required = false) String dateFrom,
                                                              @RequestParam(value = "dateTo", required = false) String dateTo) {
        HttpHeaders headers = new HttpHeaders();
        List<DeploymentDto> deployments = deploymentService.getDeployments(projectId, limit, page, dateFrom, dateTo, headers);
        return new ResponseEntity<>(deployments, headers, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DeploymentDto> getDeployment(@PathVariable("id") Long id) {
        DeploymentDto deployment = deploymentService.getDeployment(id);
        return ResponseEntity.ok(deployment);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteDeployment(@PathVariable("id") Long id) {
        Deployment deployment = deploymentService.deleteDeployment(id);
        Long projectId = deployment.getProject().getId();
        if (!deploymentService.hasDeployments(projectId)) {
            projectService.setProjectStatusPreProduction(projectId);
        }
        return ResponseEntity.noContent().build();
    }
}
