package com.devbridge.internal.devopsmetrics.dto;

import lombok.*;

@RequiredArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProgressChartDto {
    @NonNull
    private String date;
    @NonNull
    private Float average;
    private Float median;

    public ProgressChartDto(String date, Float average, Float median) {
        this.date = date;
        this.average = average;
        this.median = median;
    }
}
