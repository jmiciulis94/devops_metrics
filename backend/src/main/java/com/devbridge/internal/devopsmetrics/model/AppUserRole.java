package com.devbridge.internal.devopsmetrics.model;

public enum AppUserRole {
    ADMIN, USER
}