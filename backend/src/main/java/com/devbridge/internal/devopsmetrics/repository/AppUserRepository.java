package com.devbridge.internal.devopsmetrics.repository;

import com.devbridge.internal.devopsmetrics.model.AppUser;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface AppUserRepository extends PagingAndSortingRepository<AppUser, Long> {
    Optional<AppUser> findByEmailAndDeletedFalse(String email);
    List<AppUser> findAllByDeletedFalse();
   Optional<AppUser> findByIdAndDeletedFalse(Long id);
   List<AppUser> findAllByDeletedFalse(Pageable pageable);
}
