package com.devbridge.internal.devopsmetrics.model;

public enum ProjectStatus {
    IN_PRODUCTION, PRE_PRODUCTION
}
