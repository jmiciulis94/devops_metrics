package com.devbridge.internal.devopsmetrics.dto;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Getter
@Setter
public class SettingsDto {

    private Long id;
    @Min(0)
    @Max(100)
    private BigDecimal failureRateLimit;
    @Min(0)
    private BigDecimal deploymentFrequencyLimit;

}
