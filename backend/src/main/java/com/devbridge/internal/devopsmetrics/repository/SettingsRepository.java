package com.devbridge.internal.devopsmetrics.repository;

import com.devbridge.internal.devopsmetrics.model.Settings;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SettingsRepository extends CrudRepository<Settings, Long> {

    Settings findFirstBy();
}
