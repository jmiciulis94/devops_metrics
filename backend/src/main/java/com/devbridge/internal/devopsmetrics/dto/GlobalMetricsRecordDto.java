package com.devbridge.internal.devopsmetrics.dto;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
public class GlobalMetricsRecordDto {

    private String squadName;
    private String projectTitle;
    private Integer leadTime;
    private Integer deploymentDuration;
    private Float failureRate;
    private Float deploymentFrequency;
}
