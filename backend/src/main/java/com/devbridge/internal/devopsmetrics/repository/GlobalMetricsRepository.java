package com.devbridge.internal.devopsmetrics.repository;

import com.devbridge.internal.devopsmetrics.dto.GlobalMetricsRecordDto;
import com.devbridge.internal.devopsmetrics.util.FilteringUtil;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;
import java.util.List;

@Repository
public class GlobalMetricsRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public GlobalMetricsRepository(DataSource dataSource){
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<GlobalMetricsRecordDto> getGlobalMetricsData(String dateFrom, String dateTo,
                                                             List<Integer> squadIds,
                                                             List<Integer> projectIds){
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(dateFrom, dateTo, squadIds, projectIds);

        String sql = "SELECT squad.name,\n" +
                "       project.title,\n" +
                "       COALESCE(singleDeployment.lead_time, 0)           AS lead_time,\n" +
                "       COALESCE(singleDeployment.deployment_duration, 0) AS deployment_duration,\n" +
                "       (COUNT(CASE WHEN otherDeployment.is_defect = 'true' then 1 end) * 100)::float /\n" +
                "       COUNT(otherDeployment)                            AS failure_rate,\n" +
                "       (COUNT(otherDeployment)::float) / (CASE\n" +
                "                                              WHEN DATE_PART('day', NOW() - MIN(otherDeployment.date)) = 0 THEN 1\n" +
                "                                              ELSE DATE_PART('day', NOW() - MIN(otherDeployment.date))\n" +
                "           END)                                          AS deployment_frequency\n" +
                "FROM devops_metrics.project AS project\n" +
                "         INNER JOIN devops_metrics.squad AS squad ON squad.id = project.squad_id\n" +
                "         LEFT JOIN devops_metrics.deployment AS singleDeployment\n" +
                "                   ON project.id = singleDeployment.project_id AND singleDeployment.id = (\n" +
                "                       SELECT deployment.id\n" +
                "                       FROM devops_metrics.deployment AS deployment\n" +
                "                       WHERE deployment.project_id = project.id\n" +
                "                         AND deployment.deleted = false\n" +
                "                         AND deployment.lead_time IS NOT NULL\n" +
                "                         AND deployment.deployment_duration IS NOT NULL\n" +
                "                         AND coalesce(deployment.date >= cast(:dateFrom AS timestamp), deployment.date <> all ('{}'))\n" +
                "                         AND coalesce(deployment.date <= cast(:dateTo AS timestamp), deployment.date <> all ('{}'))\n" +
                "                       ORDER BY deployment.date DESC\n" +
                "                       LIMIT 1\n" +
                "                   )\n" +
                "         INNER JOIN devops_metrics.deployment AS otherDeployment ON project.id = otherDeployment.project_id\n" +
                "WHERE project.status = 'IN_PRODUCTION'\n" +
                "  AND project.deleted = false\n" +
                "  AND otherDeployment.deleted = false\n" +
                "  and coalesce(otherDeployment.date >= cast(:dateFrom as timestamp), otherDeployment.date <> all ('{}'))\n" +
                "  and coalesce(otherDeployment.date <= cast(:dateTo as timestamp), otherDeployment.date <> all ('{}'))\n" +
                "  AND coalesce(otherDeployment.project_id in (:projectIds), otherDeployment.project_id <> all ('{}'))\n" +
                "  AND coalesce(project.squad_id in (:squadIds), project.squad_id <> all ('{}'))\n" +
                "GROUP BY project.title, squad.name, singleDeployment.lead_time, singleDeployment.deployment_duration\n" +
                "ORDER BY title;";
        return namedParameterJdbcTemplate.query(sql, parameters,(rs, rowNum) ->
                new GlobalMetricsRecordDto(rs.getString("name"), rs.getString("title"), rs.getInt("lead_time"),
                        rs.getInt("deployment_duration"), rs.getFloat("failure_rate"),
                        rs.getFloat("deployment_frequency")));
    }

}

