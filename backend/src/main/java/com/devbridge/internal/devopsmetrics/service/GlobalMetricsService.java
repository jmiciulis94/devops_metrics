package com.devbridge.internal.devopsmetrics.service;

import com.devbridge.internal.devopsmetrics.dto.GlobalMetricsDto;
import com.devbridge.internal.devopsmetrics.dto.GlobalMetricsRecordDto;
import com.devbridge.internal.devopsmetrics.dto.GlobalMetricsStatDto;
import com.devbridge.internal.devopsmetrics.repository.GlobalMetricsRepository;
import com.devbridge.internal.devopsmetrics.util.FilteringUtil;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GlobalMetricsService {

    private final GlobalMetricsRepository globalMetricsRepository;

    public GlobalMetricsService(GlobalMetricsRepository globalMetricsRepository) {
        this.globalMetricsRepository = globalMetricsRepository;
    }

    public GlobalMetricsDto getMetrics(String dateFrom, String dateTo,
                                       List<String> squadIds, List<String> projectIds){
        List<Integer> squads = FilteringUtil.convertStringArrayToIntArray(squadIds);
        List<Integer> projects = FilteringUtil.convertStringArrayToIntArray(projectIds);

        List<GlobalMetricsRecordDto> metricsRecords = globalMetricsRepository.getGlobalMetricsData(dateFrom, dateTo, squads, projects);
        List<GlobalMetricsStatDto> metricsStats = new ArrayList<>();
        GlobalMetricsStatDto average = getAverage(metricsRecords);
        GlobalMetricsStatDto median = getMedian(metricsRecords);
        metricsStats.add(average);
        metricsStats.add(median);
        return new GlobalMetricsDto(metricsRecords, metricsStats);
    }

    public GlobalMetricsStatDto getAverage(List<GlobalMetricsRecordDto> globalMetricsRecords) {
        GlobalMetricsStatDto average = new GlobalMetricsStatDto();
        average.setName("Average");
        float deploymentDurationSum = 0;
        float deploymentFrequencySum = 0;
        float failureRateSum = 0;
        float leadTimeSum = 0;
        int size = globalMetricsRecords.size();

        if (size == 0) {
            size = 1;
        }

        for(GlobalMetricsRecordDto record : globalMetricsRecords) {
            deploymentDurationSum += record.getDeploymentDuration();
            deploymentFrequencySum += record.getDeploymentFrequency();
            failureRateSum += record.getFailureRate();
            leadTimeSum += record.getLeadTime();
        }
        average.setDeploymentDuration(deploymentDurationSum / size);
        average.setDeploymentFrequency(deploymentFrequencySum / size);
        average.setFailureRate(failureRateSum / size);
        average.setLeadTime(leadTimeSum / size);
        return average;
    }

    public GlobalMetricsStatDto getMedian(List<GlobalMetricsRecordDto> globalMetricsRecords) {
        GlobalMetricsStatDto median = new GlobalMetricsStatDto();
        median.setName("Median");
        int size = globalMetricsRecords.size();
        if (size == 0) {
            median.setDeploymentDuration(0F);
            median.setDeploymentFrequency(0F);
            median.setDeploymentDuration(0F);
            median.setFailureRate(0F);
            median.setLeadTime(0F);
        } else {
            List<Integer> deploymentDuration = globalMetricsRecords.stream().map(GlobalMetricsRecordDto::getDeploymentDuration).sorted().collect(Collectors.toList());
            List<Float> deploymentFrequency = globalMetricsRecords.stream().map(GlobalMetricsRecordDto::getDeploymentFrequency).sorted().collect(Collectors.toList());
            List<Float> failureRate = globalMetricsRecords.stream().map(GlobalMetricsRecordDto::getFailureRate).sorted().collect(Collectors.toList());
            List<Integer> leadTime = globalMetricsRecords.stream().map(GlobalMetricsRecordDto::getLeadTime).sorted().collect(Collectors.toList());
            if (size % 2 == 0) {
                median.setDeploymentDuration(((float)deploymentDuration.get(size/2) + (float)deploymentDuration.get(size / 2 - 1)) / 2);
                median.setDeploymentFrequency((deploymentFrequency.get(size/2) + deploymentFrequency.get(size / 2 - 1)) / 2);
                median.setFailureRate((failureRate.get(size/2) + failureRate.get(size / 2 - 1)) / 2);
                median.setLeadTime(((float)leadTime.get(size/2) + (float)leadTime.get(size / 2 - 1)) / 2);
            } else {
                median.setDeploymentDuration((float)deploymentDuration.get(size / 2));
                median.setDeploymentFrequency(deploymentFrequency.get(size / 2));
                median.setFailureRate(failureRate.get(size / 2));
                median.setLeadTime((float)leadTime.get(size / 2));
            }
        }
        return median;
    }
}
