package com.devbridge.internal.devopsmetrics.converter;

import java.time.format.DateTimeFormatter;

import com.devbridge.internal.devopsmetrics.dto.DeploymentDto;
import com.devbridge.internal.devopsmetrics.model.Deployment;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class DeploymentConverter {

    public Deployment dtoToEntity(DeploymentDto deploymentDto){

        ModelMapper modelMapper = new ModelMapper();
        Deployment deployment = modelMapper.map(deploymentDto, Deployment.class);
        return deployment;

    }

    public DeploymentDto convertEntityToViewDto(Deployment deployment) {
        DeploymentDto deploymentDto = new DeploymentDto();
        deploymentDto.setId(deployment.getId());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        deploymentDto.setDate(formatter.format(deployment.getDate()));
        deploymentDto.setIsDefect(deployment.getIsDefect());
        deploymentDto.setLeadTime(deployment.getLeadTime());
        deploymentDto.setDeploymentDuration(deployment.getDeploymentDuration());
        deploymentDto.setNotes(deployment.getNotes());
        return deploymentDto;
    }

}
