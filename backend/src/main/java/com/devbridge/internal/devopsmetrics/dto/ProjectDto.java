package com.devbridge.internal.devopsmetrics.dto;

import com.devbridge.internal.devopsmetrics.model.ProjectStatus;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Getter
@Setter
public class ProjectDto {

    private Long id;
    @NotBlank
    @Size(max = 255)
    private String title;
    @NotNull
    private ProjectStatus status;
    @NotNull
    private Long squadId;
    @NotBlank
    @Size(max = 255)
    private String squadName;
    @NotBlank
    private String lastDeployed;
    private String notes;

}
