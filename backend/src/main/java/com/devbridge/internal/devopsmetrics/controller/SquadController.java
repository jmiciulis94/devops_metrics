package com.devbridge.internal.devopsmetrics.controller;

import com.devbridge.internal.devopsmetrics.dto.SquadDto;
import com.devbridge.internal.devopsmetrics.exception.BadRequestException;
import com.devbridge.internal.devopsmetrics.exception.NotFoundException;
import com.devbridge.internal.devopsmetrics.model.Squad;
import com.devbridge.internal.devopsmetrics.service.SquadService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value ="/squads")
public class SquadController {
    private final SquadService squadService;

    public SquadController(SquadService squadService) {
        this.squadService = squadService;
    }

    @PostMapping
    public ResponseEntity<Squad> saveSquad(@RequestBody @Valid SquadDto squad) {
        return ResponseEntity.ok(squadService.saveSquad(squad));
    }

    @GetMapping()
    public ResponseEntity<List<SquadDto>> getSquads() {
        List<SquadDto> squads = squadService.getSquads();
        return ResponseEntity.ok(squads);
    }

    @GetMapping(params = {"limit", "page"})
    public ResponseEntity<List<SquadDto>> getSquads(@RequestParam("limit") int limit, @RequestParam("page") int page) {
        HttpHeaders headers = new HttpHeaders();
        List<SquadDto> squads = squadService.getSquads(limit, page, headers);
        return new ResponseEntity<>(squads, headers, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SquadDto> getSquad(@PathVariable("id") Long id) {
        SquadDto squad = squadService.getSquad(id);
        return ResponseEntity.ok(squad);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<SquadDto> removeSquad(@PathVariable("id") Long id) {
        SquadDto squad = squadService.deleteSquad(id);
        if (squad != null) {
            return ResponseEntity.ok().body(squad);
        } else throw new NotFoundException("Could not delete squad", "Squad");
    }

    @PutMapping("/{id}")
    public ResponseEntity<SquadDto> updateSquad(@PathVariable(value = "id") Long id,
                                                @Valid @RequestBody SquadDto squadDto) {
        if (squadDto.getId() == id) {
            return ResponseEntity.ok(squadService.updateSquad(squadDto));
        } else {
            throw new BadRequestException("Path id did not match request object id. Check url.", "Squad");
        }
    }
}