package com.devbridge.internal.devopsmetrics.dto;

import lombok.*;

@RequiredArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MedianDto {
    @NonNull
    private String date;
    @NonNull
    private Float value;
}

