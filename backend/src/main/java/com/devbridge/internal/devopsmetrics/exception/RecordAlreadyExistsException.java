package com.devbridge.internal.devopsmetrics.exception;

public class RecordAlreadyExistsException extends RuntimeException {
    private final String field;

    public RecordAlreadyExistsException(String message, String field) {
        super(message);
        this.field = field;
    }

    public String getField() {
        return field;
    }
}
