package com.devbridge.internal.devopsmetrics.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@Getter
@Setter
public class SquadDto {
    private Long id;
    @NotBlank @NotNull
    private String name;
}

