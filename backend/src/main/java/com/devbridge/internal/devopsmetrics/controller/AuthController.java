package com.devbridge.internal.devopsmetrics.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.devbridge.internal.devopsmetrics.dto.AppUserDto;
import com.devbridge.internal.devopsmetrics.dto.AuthenticationRequest;
import com.devbridge.internal.devopsmetrics.exception.WrongCredentialsException;
import com.devbridge.internal.devopsmetrics.model.AppUserInfo;
import com.devbridge.internal.devopsmetrics.model.AppUserRole;
import com.devbridge.internal.devopsmetrics.service.AppUserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;

import static com.devbridge.internal.devopsmetrics.util.SecurityConstants.*;

@RestController
@RequestMapping(value = "/auth")
public class AuthController {

    private AppUserService appUserService;
    private AuthenticationManager authenticationManager;

    public AuthController(AppUserService appUserService, AuthenticationManager authenticationManager) {
        this.appUserService = appUserService;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping("/login")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody @Valid AuthenticationRequest authenticationRequest) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            authenticationRequest.getEmail(),
                            authenticationRequest.getPassword(),
                            new ArrayList<>()
                    )
            );
            String token = JWT.create()
                    .withSubject(((User) authentication.getPrincipal()).getUsername())
                    .withClaim("Role", authentication.getAuthorities().iterator().next().getAuthority())
                    .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                    .sign(Algorithm.HMAC512(SECRET_KEY.getBytes()));

            HttpHeaders headers = new HttpHeaders();
            headers.add(HEADER_STRING, TOKEN_PREFIX + token);
            headers.add("Role", authentication.getAuthorities().iterator().next().getAuthority());
            headers.add("Access-Control-Expose-Headers", "*");
            AppUserInfo info = (AppUserInfo) authentication.getPrincipal();
            AppUserDto userInfo = new AppUserDto(info.getId(), info.getFirstName(), info.getLastName(), info.getUsername(), AppUserRole.valueOf(info.getAuthorities().iterator().next().getAuthority()));

            return new ResponseEntity<>(userInfo, headers, HttpStatus.OK);
        } catch (AuthenticationException e) {
            throw new WrongCredentialsException("Wrong email or password", "Login");
        }
    }

    @PostMapping("/refresh")
    public ResponseEntity<?> refreshToken(@RequestHeader(value = HEADER_REFRESH_STRING) String token) {
        if (token == null || !token.startsWith(TOKEN_PREFIX)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        try {
            String subject = JWT.decode(token.replace(TOKEN_PREFIX, "")).getSubject();
            String newToken = JWT.create()
                    .withSubject(subject)
                    .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                    .sign(Algorithm.HMAC512(SECRET_KEY.getBytes()));

            HttpHeaders headers = new HttpHeaders();
            headers.add(HEADER_STRING, TOKEN_PREFIX + newToken);
            headers.add("Access-Control-Expose-Headers", "*");

            return new ResponseEntity<>(headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

}
