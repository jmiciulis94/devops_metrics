package com.devbridge.internal.devopsmetrics.repository;

import com.devbridge.internal.devopsmetrics.model.Deployment;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeploymentRepository extends PagingAndSortingRepository<Deployment, Long> {
    List<Deployment> findAllByProjectIdAndDeletedFalse(Long projectId);
    Deployment findByIdAndDeletedFalse(Long id);

    @Query(value = "select count(*) from devops_metrics.deployment\n" +
            "            where deployment.deleted = false\n" +
            "              and deployment.project_id = :projectId\n" +
            "              and case when :dateFrom!='none' then deployment.date >= cast(:dateFrom as timestamp)\n" +
            "                  else deployment.date<> all('{}') end\n" +
            "              and case when :dateTo!='none' then deployment.date <= cast(:dateTo as timestamp)\n" +
            "                  else deployment.date<> all('{}') end", nativeQuery = true)
    int findFilteredCount(@Param("projectId") Long projectId,
                                  @Param("dateFrom") String dateFrom,
                                  @Param("dateTo") String dateTo
    );

    @Query(value = "select * from devops_metrics.deployment\n" +
            "where deployment.deleted = false\n" +
            "  and deployment.project_id = :projectId\n" +
            "  and case\n" +
            "      when :dateFrom!='none'\n" +
            "          then deployment.date >= cast(:dateFrom as timestamp)\n" +
            "      else deployment.date<> all('{}')\n" +
            "      end\n" +
            "  and case\n" +
            "      when :dateTo!='none'\n" +
            "          then deployment.date <= cast(:dateTo as timestamp)\n" +
            "      else deployment.date<> all('{}')\n" +
            "      end\n" +
            "order by deployment.date DESC", nativeQuery = true)
    List<Deployment> findFiltered(Pageable pageable,
                                  @Param("projectId") Long projectId,
                                  @Param("dateFrom") String dateFrom,
                                  @Param("dateTo") String dateTo
    );
}