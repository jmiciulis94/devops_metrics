package com.devbridge.internal.devopsmetrics.model;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "settings", schema="devops_metrics")
public class Settings {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "failure_rate_limit", nullable = false)
    private BigDecimal failureRateLimit;

    @Column(name = "deployment_frequency_limit", nullable = false)
    private BigDecimal deploymentFrequencyLimit;

}
