package com.devbridge.internal.devopsmetrics.repository;

import com.devbridge.internal.devopsmetrics.dto.QuadrantDto;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import com.devbridge.internal.devopsmetrics.util.FilteringUtil;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class QuadrantRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public QuadrantRepository(DataSource dataSource){
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<QuadrantDto> getQuadrantData(String dateFrom, String dateTo, List<Integer> squadIds,
                                             List<Integer> projectIds) {
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(dateFrom, dateTo, squadIds, projectIds);

        String sql = "SELECT squad.name, project.title, \n" +
                "CAST(COUNT(CASE WHEN deployment.is_defect='true' THEN 1 END) AS DECIMAL(10, 2)) / CAST(COUNT(deployment) AS DECIMAL(10, 2)) AS failure_rate,\n" +
                "CAST(COUNT(deployment) AS DECIMAL(10, 2)) / (CASE \n" +
                "WHEN cast(:dateTo as timestamp) is null THEN (CASE \n" +
                "WHEN DATE_PART('day', cast(now() as timestamp) - cast(MIN(deployment.date) as timestamp))=0 THEN 1\n" +
                "ELSE DATE_PART('day', cast(now() as timestamp) - cast(MIN(deployment.date) as timestamp))\n" +
                "END)\n" +
                "ELSE (CASE " +
                "WHEN DATE_PART('day', cast(:dateTo as timestamp) - cast(MIN(deployment.date) as timestamp))=0 THEN 1\n" +
                "ELSE DATE_PART('day', cast(:dateTo as timestamp) - cast(MIN(deployment.date) as timestamp))\n" +
                "END)END)\n" +
                "AS deployment_frequency\n" +
                "FROM devops_metrics.project AS project\n" +
                "INNER JOIN devops_metrics.deployment AS deployment ON deployment.project_id = project.id\n" +
                "INNER JOIN devops_metrics.squad AS squad ON squad.id = project.squad_id\n" +
                "WHERE project.deleted='false' AND squad.deleted='false' AND deployment.deleted='false'\n" +
                "and coalesce(deployment.date >= cast(:dateFrom as timestamp), deployment.date<> all('{}'))\n"+
                "and coalesce(deployment.date <= cast(:dateTo as timestamp), deployment.date<> all('{}'))\n"+
                "and coalesce(project.id in (:projectIds), project.id<> all('{}'))\n"+
                "and coalesce(project.squad_id in (:squadIds), project.squad_id<> all('{}'))\n"+
                "GROUP BY squad.name, project.title\n";
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new QuadrantDto(rs.getString("name"), rs.getString("title"), rs.getBigDecimal("failure_rate"), rs.getBigDecimal("deployment_frequency"))
        );
    }

}
