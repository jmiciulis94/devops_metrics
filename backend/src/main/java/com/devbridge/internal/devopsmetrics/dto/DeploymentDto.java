package com.devbridge.internal.devopsmetrics.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
@Getter
@Setter
public class DeploymentDto {

    private Long id;
    @NotNull
    private String date;
    private Boolean isDefect = false;
    private String notes;
    @Min(1)
    private Long leadTime;
    @Min(1)
    private Long deploymentDuration;
    private Long projectId;
}

