package com.devbridge.internal.devopsmetrics.service;

import com.devbridge.internal.devopsmetrics.dto.SquadDto;
import com.devbridge.internal.devopsmetrics.exception.RecordAlreadyExistsException;
import com.devbridge.internal.devopsmetrics.exception.CannotDeleteException;
import com.devbridge.internal.devopsmetrics.exception.NotFoundException;
import com.devbridge.internal.devopsmetrics.model.Project;
import com.devbridge.internal.devopsmetrics.model.Squad;
import com.devbridge.internal.devopsmetrics.repository.ProjectRepository;
import com.devbridge.internal.devopsmetrics.repository.SquadRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SquadService {
    private final SquadRepository squadRepository;
    private final ProjectRepository projectRepository;

    public SquadService(SquadRepository squadRepository, ProjectRepository projectRepository) {
        this.squadRepository = squadRepository;
        this.projectRepository = projectRepository;
    }

    public Squad saveSquad(SquadDto squadDto) {
        ModelMapper modelMapper = new ModelMapper();
        Squad squad = modelMapper.map(squadDto, Squad.class);
        checkIfExists(squad.getName(), "create");
        squadRepository.save(squad);
        return squad;
    }

    public List<SquadDto> getSquads() {
        List<Squad> squads = squadRepository.findAllByDeletedFalseOrderByName();
        return new ModelMapper().map(squads, new TypeToken<List<SquadDto>>() {}.getType());
    }

    public List<SquadDto> getSquads(int limit, int page, HttpHeaders headers) {
        String count = String.format("%d", squadRepository.findAllByDeletedFalseOrderByName().size());
        headers.add("X-Total-Count", count);
        headers.add("Access-Control-Expose-Headers", "*");
        Pageable pageable = PageRequest.of(page, limit);
        List<Squad> squads = squadRepository.findAllByDeletedFalseOrderByName(pageable);
        return new ModelMapper().map(squads, new TypeToken<List<SquadDto>>() {
        }.getType());
    }

    public SquadDto getSquad(Long id) {
        Squad squad = squadRepository.findByIdAndDeletedFalse(id);
        if (squad == null) throw new NotFoundException(String.format("Squad with id=%d not found", id), "Squad");
        ModelMapper modelMapper = new ModelMapper();
        SquadDto squadDto = modelMapper.map(squad, SquadDto.class);
        return squadDto;
    }

    public SquadDto deleteSquad(Long id) {
        Optional<Squad> squad = squadRepository.findById(id);
        if (squad.isEmpty() || squad.get().isDeleted()) {
            return null;
        } else {
            Squad squadToDelete = squad.get();
            List<Project> projects = projectRepository.findAllBySquadIdAndDeletedFalse(squadToDelete.getId());
            if (!projects.isEmpty()) {
                throw new CannotDeleteException("The squad could not be deleted. It has assigned projects.", "Squad");
            }
            squadToDelete.setDeleted(true);
            squadRepository.save(squadToDelete);
            return new ModelMapper().map(squadToDelete, SquadDto.class);
        }
    }

    public SquadDto updateSquad(SquadDto squadDto) {
        Squad squad = squadRepository.findByIdAndDeletedFalse(squadDto.getId());
        if (squad == null) throw new NotFoundException(String.format("Squad with id=%d not found",
                squadDto.getId()), "Squad");
        if (!squadDto.getName().equals(squad.getName()))
            checkIfExists(squadDto.getName(), "update");
        squad.setName(squadDto.getName());
        squadRepository.save(squad);
        return squadDto;
    }

    private void checkIfExists(String name, String action) {
        boolean doesExist = squadRepository.existsByNameAndDeletedFalse(name);
        String message = String.format("Cannot %s squad, this squad already exists.", action);
        if (doesExist) {
            throw new RecordAlreadyExistsException(message, "Squad");
        }
    }
}