package com.devbridge.internal.devopsmetrics.controller;

import com.devbridge.internal.devopsmetrics.dto.GlobalMetricsDto;
import com.devbridge.internal.devopsmetrics.dto.ProgressChartDto;
import com.devbridge.internal.devopsmetrics.dto.QuadrantDto;
import com.devbridge.internal.devopsmetrics.exception.BadRequestException;
import com.devbridge.internal.devopsmetrics.service.GlobalMetricsService;
import com.devbridge.internal.devopsmetrics.service.ProgressChartService;
import com.devbridge.internal.devopsmetrics.service.QuadrantService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value ="/global")
public class GlobalMetricsController {

    private final GlobalMetricsService globalMetricsService;

    private final ProgressChartService progressChartService;

    private final QuadrantService quadrantService;

    public GlobalMetricsController(GlobalMetricsService globalMetricsService, ProgressChartService progressChartService,
                                   QuadrantService quadrantService) {
        this.globalMetricsService = globalMetricsService;
        this.progressChartService = progressChartService;
        this.quadrantService = quadrantService;
    }

    @GetMapping(value = "/metrics")
    public ResponseEntity<GlobalMetricsDto> getGlobalMetricsData(
            @RequestParam(name = "dateFrom", required = false) String dateFrom,
            @RequestParam(name = "dateTo", required = false) String dateTo,
            @RequestParam(name = "squadIds", required = false) List<String> squadIds,
            @RequestParam(name = "projectIds", required = false) List<String> projectIds) {
        GlobalMetricsDto globalMetrics = globalMetricsService.getMetrics(dateFrom, dateTo, squadIds, projectIds);
        return ResponseEntity.ok(globalMetrics);
    }

    @GetMapping(value = "/quadrant")
    public ResponseEntity<List<QuadrantDto>> getQuadrantData(
            @RequestParam(name = "dateFrom", required = false) String dateFrom,
            @RequestParam(name = "dateTo", required = false) String dateTo,
            @RequestParam(name = "squadIds", required = false) List<String> squadIds,
            @RequestParam(name = "projectIds", required = false) List<String> projectIds) {
        List<QuadrantDto> quadrantDtoList = quadrantService.getQuadrantData(dateFrom, dateTo, squadIds, projectIds);
        return ResponseEntity.ok(quadrantDtoList);
    }

    @GetMapping(value = "/progress/lead-time")
    public ResponseEntity<List<ProgressChartDto>> getProgressLeadTimeData(
            @RequestParam(name = "dateFrom", required = false) String from,
            @RequestParam(name = "dateTo", required = false) String to,
            @RequestParam (name="period") String period,
            @RequestParam(name = "squadIds", required = false) List<String> squadIds,
            @RequestParam(name = "projectIds", required = false) List<String> projectIds) {
        List<ProgressChartDto> leadTimeData;
        if (period == null) {
            throw new BadRequestException("The parameters to get chart are incorrect", "Progress");
        } else {
            leadTimeData = progressChartService.getLeadTime(from, to, period, squadIds, projectIds);
        }
        return ResponseEntity.ok(leadTimeData);
    }

    @GetMapping(value = "/progress/deployment-duration")
    public ResponseEntity<List<ProgressChartDto>> getDeploymentDurationData(
            @RequestParam(name = "dateFrom", required = false) String from,
            @RequestParam(name = "dateTo", required = false) String to,
            @RequestParam (name="period") String period,
            @RequestParam(name = "squadIds", required = false) List<String> squadIds,
            @RequestParam(name = "projectIds", required = false) List<String> projectIds) {
        List<ProgressChartDto> deploymentDurationData;
        if (period == null) {
            throw new BadRequestException("The parameters to get chart are incorrect", "Progress");
        } else {
            deploymentDurationData = progressChartService.getDeploymentDuration(from, to, period, squadIds, projectIds);
        }
        return ResponseEntity.ok(deploymentDurationData);
    }

    @GetMapping(value = "/progress/deployment-frequency")
    public ResponseEntity<List<ProgressChartDto>> getProgressDeploymentFrequencyData(
            @RequestParam(name = "dateFrom", required = false) String from,
            @RequestParam(name = "dateTo", required = false) String to,
            @RequestParam (name="period") String period,
            @RequestParam(name = "squadIds", required = false) List<String> squadIds,
            @RequestParam(name = "projectIds", required = false) List<String> projectIds) {
        List<ProgressChartDto> deploymentFrequencyData;
        if (period == null) {
            throw new BadRequestException("The parameters to get chart are incorrect", "Progress");
        } else {
            deploymentFrequencyData = progressChartService.getDeploymentFrequency(from, to, period, squadIds, projectIds);
        }
        return ResponseEntity.ok(deploymentFrequencyData);
    }

    @GetMapping(value = "/progress/failure-rate")
    public ResponseEntity<List<ProgressChartDto>> getProgressFailureRateData(
            @RequestParam(name = "dateFrom", required = false) String from,
            @RequestParam(name = "dateTo", required = false) String to,
            @RequestParam (name="period") String period,
            @RequestParam(name = "squadIds", required = false) List<String> squadIds,
            @RequestParam(name = "projectIds", required = false) List<String> projectIds) {
        List<ProgressChartDto> failureRateData;
        if (period == null) {
            throw new BadRequestException("The parameters to get chart are incorrect", "Progress");
        } else {
            failureRateData = progressChartService.getFailureRate(from, to, period, squadIds, projectIds);
        }
        return ResponseEntity.ok(failureRateData);
    }

}
