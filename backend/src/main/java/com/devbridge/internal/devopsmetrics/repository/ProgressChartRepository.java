package com.devbridge.internal.devopsmetrics.repository;

import com.devbridge.internal.devopsmetrics.dto.MedianDto;
import com.devbridge.internal.devopsmetrics.dto.ProgressChartDto;
import com.devbridge.internal.devopsmetrics.util.FilteringUtil;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class ProgressChartRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public ProgressChartRepository(DataSource dataSource) {
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<ProgressChartDto> getDeploymentFrequencyDay(String from, String to, List<Integer> squadIds, List<Integer> projectIds) {
        String period = "day";
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, squadIds, projectIds);
        String sql = "SELECT dates::date, COALESCE(deployments.average,0) AS average\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   interval '1 day')  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT \n" +
                "\tCOUNT(deployment) AS average, \n" +
                "\tDATE_TRUNC('day',deployment.date) AS date_deployment\n" +
                "\tFROM devops_metrics.deployment\n" +
                "\tINNER JOIN devops_metrics.project on deployment.project_id = project.id \n" +
                "\tWHERE deployment.deleted=false\n" +
                "\tAND COALESCE(deployment.project_id IN (:projectIds), deployment.project_id<>all('{}'))\n" +
                "\tAND COALESCE(project.squad_id IN (:squadIds), project.squad_id<>all('{}'))\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tGROUP BY DATE_TRUNC('day',deployment.date)\n" +
                "\tORDER BY DATE_TRUNC('day',deployment.date)\n" +
                ") AS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates::date DESC\n" +
                "LIMIT 30";
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new ProgressChartDto(rs.getString("dates"), rs.getFloat("average"), rs.getFloat("average"))
        );
    }

    public List<ProgressChartDto> getDeploymentFrequencyMonth(String from, String to, List<Integer> squadIds, List<Integer> projectIds) {
        String period = "month";
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, squadIds, projectIds);
        String sql = "SELECT dates::date, COALESCE(deployments.average,0) AS average\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   interval '1 month')  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT \n" +
                "\tCOUNT(deployment)::float / \n" +
                "\tEXTRACT(DAYS FROM DATE_TRUNC('month',deployment.date) + INTERVAL '1 month - 1 day')::float AS average, \n" +
                "\tDATE_TRUNC('month',deployment.date) AS date_deployment\n" +
                "\tFROM devops_metrics.deployment\n" +
                "\tINNER JOIN devops_metrics.project on deployment.project_id = project.id \n" +
                "\tWHERE deployment.deleted=false\n" +
                "\tAND COALESCE(deployment.project_id IN (:projectIds), deployment.project_id<>all('{}'))\n" +
                "\tAND COALESCE(project.squad_id IN (:squadIds), project.squad_id<>all('{}'))\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tGROUP BY DATE_TRUNC('month',deployment.date)\n" +
                "\tORDER BY DATE_TRUNC('month',deployment.date)\n" +
                ") AS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates::date DESC\n" +
                "LIMIT 12";
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new ProgressChartDto(rs.getString("dates"), rs.getFloat("average"))
        );
    }

    public List<ProgressChartDto> getDeploymentFrequencyYear(String from, String to, List<Integer> squadIds, List<Integer> projectIds) {
        String period = "year";
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, squadIds, projectIds);
        String sql = "SELECT dates::date, COALESCE(deployments.average,0) AS average\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   interval '1 year')  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT \n" +
                "\tCOUNT(deployment)::float / 365 AS average, \n" +
                "\tDATE_TRUNC('year',deployment.date) AS date_deployment\n" +
                "\tFROM devops_metrics.deployment\n" +
                "\tINNER JOIN devops_metrics.project on deployment.project_id = project.id \n" +
                "\tWHERE deployment.deleted=false\n" +
                "\tAND COALESCE(deployment.project_id IN (:projectIds), deployment.project_id<>all('{}'))\n" +
                "\tAND COALESCE(project.squad_id IN (:squadIds), project.squad_id<>all('{}'))\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tGROUP BY DATE_TRUNC('year',deployment.date)\n" +
                "\tORDER BY DATE_TRUNC('year',deployment.date)\n" +
                ") AS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates::date DESC\n" +
                "LIMIT 5";
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new ProgressChartDto(rs.getString("dates"), rs.getFloat("average"))
        );
    }
    public List<MedianDto> getDeploymentFrequencyMedianData(String period, String from, String to, List<Integer> squadIds, List<Integer> projectIds) {
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, squadIds, projectIds);
        String sql = "SELECT DATE_TRUNC(:period, dates)::date AS dates, COALESCE(deployments.data_median, 0) AS data_median\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   interval '1 day')  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT \n" +
                "\tCOUNT(deployment) AS data_median, \n" +
                "\tDATE_TRUNC('day', deployment.date) AS date_deployment\n" +
                "\tFROM devops_metrics.deployment\n" +
                "\tINNER JOIN devops_metrics.project on deployment.project_id = project.id \n" +
                "\tWHERE deployment.deleted=false\n" +
                "\tAND COALESCE(deployment.project_id IN (:projectIds), deployment.project_id<>all('{}'))\n" +
                "\tAND COALESCE(project.squad_id IN (:squadIds), project.squad_id<>all('{}'))\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tGROUP BY DATE_TRUNC('day',deployment.date)\n" +
                "\tORDER BY DATE_TRUNC('day',deployment.date)\n" +
                ") AS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates DESC, data_median";
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new MedianDto(rs.getString("dates"), rs.getFloat("data_median"))
        );
    }

    public List<ProgressChartDto> getLeadTime(String period, String from, String to, List<Integer> squadIds, List<Integer> projectIds) {
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, squadIds, projectIds);
        String sql = String.format("SELECT DATE_TRUNC(:period,dates)::date AS dates, COALESCE(deployments.average,-1) AS average\n" +
                " FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   concat('1', ' ', :period)::interval)  AS dates\n" +
                " LEFT JOIN(\n" +
                "\t SELECT DATE_TRUNC('%s', deployment.date) AS date_deployment, AVG(deployment.lead_time) AS average\n" +
                "\t FROM devops_metrics.deployment AS deployment\n" +
                "\t INNER JOIN devops_metrics.project on deployment.project_id = project.id \n" +
                "\t WHERE deployment.deleted=false\n" +
                "\t AND COALESCE(deployment.project_id IN (:projectIds), deployment.project_id<>all('{}'))\n" +
                "\t AND COALESCE(project.squad_id IN (:squadIds), project.squad_id<>all('{}'))\n" +
                " AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                " AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\t GROUP BY DATE_TRUNC('%s',deployment.date)\n" +
                "\t ORDER BY DATE_TRUNC('%s',deployment.date)\n" +
                ") \n" +
                "\t AS deployments ON deployments.date_deployment=dates\n" +
                " ORDER BY dates::date DESC\n" +
                " LIMIT CASE \n" +
                "\t WHEN :period = 'day' THEN 30\n" +
                "\t WHEN :period = 'month' THEN 12\n" +
                "\t ELSE 5 END;", period, period, period);
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new ProgressChartDto(rs.getString("dates"), rs.getFloat("average")));
    }

    public List<MedianDto> getLeadTimeMedianData(String period, String from, String to, List<Integer> squadIds, List<Integer> projectIds) {
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, squadIds, projectIds);
        String sql = "SELECT (CASE WHEN :period='day' THEN dates::date " +
                "\t\t ELSE DATE_TRUNC(:period, dates)::date END) AS dates,\n" +
                "COALESCE(deployments.data_median,-1) AS data_median\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "\t\t date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "\t\t coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   interval '1 day')  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT DATE_TRUNC('day', deployment.date)::date AS date_deployment, deployment.lead_time AS data_median\n" +
                "\tFROM devops_metrics.deployment AS deployment\n" +
                "\tINNER JOIN devops_metrics.project on deployment.project_id = project.id \n" +
                "\tWHERE deployment.deleted=false\n" +
                "\t AND COALESCE(deployment.project_id IN (:projectIds), deployment.project_id<>all('{}'))\n" +
                "\t AND COALESCE(project.squad_id IN (:squadIds), project.squad_id<>all('{}'))\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                ") \n" +
                "\tAS deployments ON deployments.date_deployment=dates\n" +
                "\tORDER BY dates DESC, deployments.data_median";
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new MedianDto(rs.getString("dates"), rs.getFloat("data_median"))
        );
    }

    public List<ProgressChartDto> getDeploymentDuration(String period, String from, String to, List<Integer> squadIds, List<Integer> projectIds) {
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, squadIds, projectIds);
        String sql = String.format("SELECT (CASE WHEN :period='year' THEN DATE_TRUNC(:period, dates)::date " +
                "\t\t ELSE dates::date END) AS dates,\n" +
                "COALESCE(deployments.average,-1) AS average\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "\t\t\tdate_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "\t\t\tcoalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   concat('1', ' ', :period)::interval)  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT DATE_TRUNC('%s', deployment.date)::date AS date_deployment, AVG(deployment.deployment_duration) AS average\n" +
                "\tFROM devops_metrics.deployment AS deployment\n" +
                "\tINNER JOIN devops_metrics.project on deployment.project_id = project.id \n" +
                "\tWHERE deployment.deleted=false\n" +
                "\t AND COALESCE(deployment.project_id IN (:projectIds), deployment.project_id<>all('{}'))\n" +
                "\t AND COALESCE(project.squad_id IN (:squadIds), project.squad_id<>all('{}'))\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tGROUP BY DATE_TRUNC('%s',deployment.date)::date\n" +
                "\tORDER BY DATE_TRUNC('%s',deployment.date)::date\n" +
                ") \n" +
                "\tAS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates::date DESC\n" +
                "LIMIT CASE\n" +
                "\tWHEN :period='day' THEN 30\n" +
                "\tWHEN :period='month' THEN 12\n" +
                "\tELSE 5 END;", period, period, period);
        return namedParameterJdbcTemplate.query(sql,parameters, (rs, rowNum) -> new ProgressChartDto(rs.getString("dates"), rs.getFloat("average"))
        );
    }


    public List<MedianDto> getDeploymentDurationMedianData(String period, String from, String to, List<Integer> squadIds, List<Integer> projectIds) {
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, squadIds, projectIds);
        String sql = "SELECT (CASE WHEN :period='day' THEN dates::date " +
                "\t\t ELSE DATE_TRUNC(:period, dates)::date END) AS dates,\n" +
                "COALESCE(deployments.data_median,-1) AS data_median\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   interval '1 day')  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT DATE_TRUNC('day', deployment.date)::date AS date_deployment, deployment.deployment_duration AS data_median\n" +
                "\tFROM devops_metrics.deployment AS deployment\n" +
                "\tINNER JOIN devops_metrics.project on deployment.project_id = project.id \n" +
                "\tWHERE deployment.deleted=false\n" +
                "\tAND COALESCE(deployment.project_id IN (:projectIds), deployment.project_id<>all('{}'))\n" +
                "\tAND COALESCE(project.squad_id IN (:squadIds), project.squad_id<>all('{}'))\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tORDER BY DATE_TRUNC('day',deployment.date)\n" +
                ") \n" +
                "\tAS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates::date DESC, data_median";
        return namedParameterJdbcTemplate.query(sql, parameters,(rs, rowNum) -> new MedianDto(rs.getString("dates"), rs.getFloat("data_median"))
        );
    }

    public List<ProgressChartDto> getFailureRate(String period, String from, String to, List<Integer> squadIds, List<Integer> projectIds) {
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, squadIds, projectIds);
        String sql = String.format("SELECT dates::date AS dates, COALESCE(deployments.average,0) AS average\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   concat('1', ' ', :period)::interval) AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT DATE_TRUNC('%s', deployment.date)::date AS date_deployment, \n" +
                "\tCOUNT(CASE WHEN deployment.is_defect='true' then 1 end) * 100::float / COUNT(deployment)::float AS average\n" +
                "\tFROM devops_metrics.deployment AS deployment\n" +
                "\tINNER JOIN devops_metrics.project on deployment.project_id = project.id \n" +
                "\tWHERE deployment.deleted=false\n" +
                "\t AND COALESCE(deployment.project_id IN (:projectIds), deployment.project_id<>all('{}'))\n" +
                "\t AND COALESCE(project.squad_id IN (:squadIds), project.squad_id<>all('{}'))\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tGROUP BY DATE_TRUNC('%s',deployment.date)\n" +
                "\tORDER BY DATE_TRUNC('%s',deployment.date)\n" +
                ") \n" +
                "\tAS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates::date DESC\n" +
                "LIMIT CASE\n" +
                "\tWHEN :period='day' THEN 30\n" +
                "\tWHEN :period='month' THEN 12\n" +
                "\tELSE 5 END", period, period, period);
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new ProgressChartDto(rs.getString("dates"), rs.getFloat("average"))
        );
    }

    public List<MedianDto> getFailureRateMedianData(String period, String from, String to, List<Integer> squadIds, List<Integer> projectIds) {
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, squadIds, projectIds);
        String sql = "SELECT DATE_TRUNC(:period, dates)::date AS dates, COALESCE(deployments.data_median,-1) AS data_median\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   interval '1 day')  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT DATE_TRUNC('day', deployment.date)::date AS date_deployment, \n" +
                "\tCOUNT(CASE WHEN deployment.is_defect='true' then 1 end) * 100::float / COUNT(deployment)::float AS data_median\n" +
                "\tFROM devops_metrics.deployment AS deployment\n" +
                "\tINNER JOIN devops_metrics.project on deployment.project_id = project.id \n" +
                "\tWHERE deployment.deleted=false\n" +
                "\t AND COALESCE(deployment.project_id IN (:projectIds), deployment.project_id<>all('{}'))\n" +
                "\t AND COALESCE(project.squad_id IN (:squadIds), project.squad_id<>all('{}'))\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tGROUP BY DATE_TRUNC('day',deployment.date)\n" +
                "\tORDER BY DATE_TRUNC('day',deployment.date)\n" +
                ") \n" +
                "\tAS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates DESC, data_median";
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new MedianDto(rs.getString("dates"), rs.getFloat("data_median"))
        );
    }

    // With IDs

    public List<ProgressChartDto> getDeploymentFrequencyDay(String from, String to, Long id) {
        String period = "day";
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, id);
        String sql = "SELECT dates::date, COALESCE(deployments.average,0) AS average\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   interval '1 day')  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT \n" +
                "\tCOUNT(deployment) AS average, \n" +
                "\tDATE_TRUNC('day',deployment.date) AS date_deployment\n" +
                "\tFROM devops_metrics.deployment\n" +
                "\tWHERE deployment.deleted=false\n" +
                "\t AND deployment.project_id=:projectId\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tGROUP BY DATE_TRUNC('day',deployment.date)\n" +
                "\tORDER BY DATE_TRUNC('day',deployment.date)\n" +
                ") AS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates::date DESC\n" +
                "LIMIT 30";
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new ProgressChartDto(rs.getString("dates"), rs.getFloat("average"), rs.getFloat("average"))
        );
    }

    public List<ProgressChartDto> getDeploymentFrequencyMonth(String from, String to, Long id) {
        String period = "month";
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, id);
        String sql = "SELECT dates::date, COALESCE(deployments.average,0) AS average\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   interval '1 month')  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT \n" +
                "\tCOUNT(deployment)::float / \n" +
                "\tEXTRACT(DAYS FROM DATE_TRUNC('month',deployment.date) + INTERVAL '1 month - 1 day')::float AS average, \n" +
                "\tDATE_TRUNC('month',deployment.date) AS date_deployment\n" +
                "\tFROM devops_metrics.deployment\n" +
                "\tWHERE deployment.deleted=false\n" +
                "\t AND deployment.project_id=:projectId\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tGROUP BY DATE_TRUNC('month',deployment.date)\n" +
                "\tORDER BY DATE_TRUNC('month',deployment.date)\n" +
                ") AS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates::date DESC\n" +
                "LIMIT 12";
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new ProgressChartDto(rs.getString("dates"), rs.getFloat("average"))
        );
    }

    public List<ProgressChartDto> getDeploymentFrequencyYear(String from, String to, Long id) {
        String period = "year";
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, id);
        String sql = "SELECT dates::date AS dates, COALESCE(deployments.average,0) AS average\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   interval '1 year')  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT \n" +
                "\tCOUNT(deployment)::float / 365 AS average, \n" +
                "\tDATE_TRUNC('year',deployment.date) AS date_deployment\n" +
                "\tFROM devops_metrics.deployment\n" +
                "\tWHERE deployment.deleted=false\n" +
                "\t AND deployment.project_id=:projectId\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tGROUP BY DATE_TRUNC('year',deployment.date)\n" +
                "\tORDER BY DATE_TRUNC('year',deployment.date)\n" +
                ") AS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates::date DESC\n" +
                "LIMIT 5";
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new ProgressChartDto(rs.getString("dates"), rs.getFloat("average"))
        );
    }

    public List<MedianDto> getDeploymentFrequencyMedianData(String period, String from, String to, Long id) {
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, id);
        String sql = "SELECT DATE_TRUNC(:period, dates)::date AS dates, COALESCE(deployments.data_median,0) AS data_median\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   interval '1 day')  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT \n" +
                "\tCOUNT(deployment) AS data_median, \n" +
                "\tDATE_TRUNC('day', deployment.date) AS date_deployment\n" +
                "\tFROM devops_metrics.deployment\n" +
                "\tWHERE deployment.deleted=false\n" +
                "\t AND deployment.project_id=:projectId\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tGROUP BY DATE_TRUNC('day',deployment.date)\n" +
                "\tORDER BY DATE_TRUNC('day',deployment.date)\n" +
                ") AS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates DESC, data_median";
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new MedianDto(rs.getString("dates"), rs.getFloat("data_median"))
        );
    }

    public List<ProgressChartDto> getLeadTime(String period, String from, String to, Long id) {
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, id);
        String sql = String.format("SELECT dates::date, COALESCE(deployments.average,-1) AS average\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   concat('1', ' ', :period)::interval)  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT DATE_TRUNC('%s', deployment.date)::date AS date_deployment, AVG(deployment.lead_time) AS average\n" +
                "\tFROM devops_metrics.deployment AS deployment\n" +
                "\tWHERE deployment.deleted=false\n" +
                "\t AND deployment.project_id=:projectId\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tGROUP BY DATE_TRUNC('%s',deployment.date)\n" +
                "\tORDER BY DATE_TRUNC('%s',deployment.date)\n" +
                ") \n" +
                "\tAS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates::date DESC\n" +
                "LIMIT CASE\n" +
                "\tWHEN :period='day' THEN 30\n" +
                "\tWHEN :period='month' THEN 12\n" +
                "\tELSE 5 END", period, period, period);
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new ProgressChartDto(rs.getString("dates"), rs.getFloat("average")));
    }

    public List<MedianDto> getLeadTimeMedianData(String period, String from, String to, Long id) {
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, id);
        String sql = "SELECT DATE_TRUNC(:period, dates)::date AS dates, COALESCE(deployments.data_median,-1) AS data_median\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   interval '1 day')  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT DATE_TRUNC('day', deployment.date)::date AS date_deployment, deployment.lead_time AS data_median\n" +
                "\tFROM devops_metrics.deployment AS deployment\n" +
                "\tWHERE deployment.deleted=false\n" +
                "\t AND deployment.project_id=:projectId\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                ") \n" +
                "\tAS deployments ON deployments.date_deployment=dates\n" +
                "\tORDER BY dates DESC, deployments.data_median";
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new MedianDto(rs.getString("dates"), rs.getFloat("data_median"))
        );
    }

    public List<ProgressChartDto> getDeploymentDuration(String period, String from, String to, Long id) {
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, id);
        String sql = String.format("SELECT dates::date, COALESCE(deployments.average,-1) AS average\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t    concat('1', ' ', :period)::interval)  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT DATE_TRUNC('%s', deployment.date)::date AS date_deployment, AVG(deployment.deployment_duration) AS average\n" +
                "\tFROM devops_metrics.deployment AS deployment\n" +
                "\tWHERE deployment.deleted=false\n" +
                "\t AND deployment.project_id=:projectId\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tGROUP BY DATE_TRUNC('%s',deployment.date)\n" +
                "\tORDER BY DATE_TRUNC('%s',deployment.date)\n" +
                ") \n" +
                "\tAS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates::date DESC\n" +
                "LIMIT CASE\n" +
                "\tWHEN :period='day' THEN 30\n" +
                "\tWHEN :period='month' THEN 12\n" +
                "\tELSE 5 END", period, period, period);
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new ProgressChartDto(rs.getString("dates"), rs.getFloat("average"))
        );
    }

    public List<MedianDto> getDeploymentDurationMedianData(String period, String from, String to, Long id) {
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, id);
        String sql = "SELECT CASE WHEN :period='day' THEN dates::date ELSE DATE_TRUNC(:period, dates)::date END AS dates, " +
                "COALESCE(deployments.data_median,-1) AS data_median\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                " date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                " coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   interval '1 day')  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT DATE_TRUNC('day', deployment.date)::date AS date_deployment, deployment.deployment_duration AS data_median\n" +
                "\tFROM devops_metrics.deployment AS deployment\n" +
                "\tWHERE deployment.deleted=false\n" +
                "\t AND deployment.project_id=:projectId\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                ") \n" +
                "\tAS deployments ON deployments.date_deployment=dates\n" +
                "\tORDER BY dates DESC, deployments.data_median";
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new MedianDto(rs.getString("dates"), rs.getFloat("data_median"))
        );
    }

    public List<ProgressChartDto> getFailureRate(String period, String from, String to, Long id) {
        MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, id);
        String sql = String.format("SELECT dates::date, COALESCE(deployments.average,0) AS average\n" +
                "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                "                              date_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                "                     coalesce(:dateTo::timestamp, NOW()),\n" +
                "\t\t\t\t\t\t   concat('1', ' ', :period)::interval)  AS dates\n" +
                "LEFT JOIN(\n" +
                "\tSELECT DATE_TRUNC('%s', deployment.date)::date AS date_deployment, \n" +
                "\tCOUNT(CASE WHEN deployment.is_defect='true' then 1 end) * 100::float / COUNT(deployment)::float AS average\n" +
                "\tFROM devops_metrics.deployment AS deployment\n" +
                "\tWHERE deployment.deleted=false\n" +
                "\t AND deployment.project_id=:projectId\n" +
                "AND COALESCE(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                "AND COALESCE(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                "\tGROUP BY DATE_TRUNC('%s',deployment.date)\n" +
                "\tORDER BY DATE_TRUNC('%s',deployment.date)\n" +
                ") \n" +
                "\tAS deployments ON deployments.date_deployment=dates\n" +
                "ORDER BY dates::date DESC\n" +
                "LIMIT CASE\n" +
                "\tWHEN :period='day' THEN 30\n" +
                "\tWHEN :period='month' THEN 12\n" +
                "\tELSE 5 END", period, period, period);
        return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new ProgressChartDto(rs.getString("dates"), rs.getFloat("average"), rs.getFloat("average"))
        );
    }

    public List<MedianDto> getFailureRateMedianData(String period, String from, String to, Long id) {
            MapSqlParameterSource parameters = FilteringUtil.mapSqlParameters(period, from, to, id);
            String sql = "SELECT DATE_TRUNC(:period, dates)::date AS dates, COALESCE(deployments.data_median,-1) AS data_median\n" +
                    "FROM generate_series(coalesce(date_trunc(:period, :dateFrom::timestamp), " +
                    "\t\tdate_trunc(:period, (select MIN(deployment.date) from devops_metrics.deployment))),\n" +
                    " \t\tcoalesce(:dateTo::timestamp, NOW()),\n" +
                    "\t\tinterval '1 day')  AS dates\n" +
                    "LEFT JOIN(\n" +
                    "\tSELECT DATE_TRUNC('day', deployment.date)::date AS date_deployment,\n" +
                    "\t\tCOUNT(CASE WHEN deployment.is_defect='true' then 1 end) * 100::float\n" +
                    "\t\t / COUNT(deployment)::float AS data_median\n" +
                    "FROM devops_metrics.deployment AS deployment\n" +
                    "\tWHERE deployment.deleted=false\n" +
                    "\t\tAND deployment.project_id=:projectId\n" +
                    "\t\tand coalesce(deployment.date >= :dateFrom::timestamp, deployment.date <> all ('{}'))\n" +
                    "\t\tand coalesce(deployment.date <= :dateTo::timestamp, deployment.date <> all ('{}'))\n" +
                    "GROUP BY DATE_TRUNC('day',deployment.date)\n" +
                    "ORDER BY DATE_TRUNC('day',deployment.date)\n" +
                    ")\n" +
                    "AS deployments ON deployments.date_deployment=dates\n" +
                    "ORDER BY dates DESC, data_median;";
            return namedParameterJdbcTemplate.query(sql, parameters, (rs, rowNum) -> new MedianDto(rs.getString("dates"), rs.getFloat("data_median"))
            );
        }

    public Float getFirst(String from, String metric) {
        String sql = String.format("SELECT %s\n" +
                "FROM devops_metrics.deployment \n" +
                "WHERE deployment.date < '%s' AND %s IS NOT NULL\n" +
                "LIMIT 1", metric, from, metric);
        List<Float> first = namedParameterJdbcTemplate.query(sql, (rs, rowNum) -> rs.getFloat(metric));
        return first.size() != 0 ? first.get(0) : Float.valueOf(0);
    }

    public Float getFirst(String from, String metric, Long id) {
        String sql = String.format("SELECT %s\n" +
                "FROM devops_metrics.deployment \n" +
                "WHERE deployment.date < '%s' AND %s IS NOT NULL AND project_id=%s\n" +
                "LIMIT 1", metric, from, metric, id);
        List<Float> first = namedParameterJdbcTemplate.query(sql, (rs, rowNum) -> rs.getFloat(metric));
        return first.size() != 0 ? first.get(0) : Float.valueOf(0);
    }

}

