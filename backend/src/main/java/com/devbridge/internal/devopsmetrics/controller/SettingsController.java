package com.devbridge.internal.devopsmetrics.controller;

import com.devbridge.internal.devopsmetrics.dto.ProjectFormDto;
import com.devbridge.internal.devopsmetrics.dto.SettingsDto;
import com.devbridge.internal.devopsmetrics.dto.SquadDto;
import com.devbridge.internal.devopsmetrics.service.SettingsService;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value ="/settings")
public class SettingsController {

    private final SettingsService settingsService;

    public SettingsController(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    @PostMapping
    public ResponseEntity<SettingsDto> updateSettings(@RequestBody @Valid SettingsDto settingsDto) {
        return ResponseEntity.ok(settingsService.updateSettings(settingsDto));
    }

    @GetMapping
    public ResponseEntity<SettingsDto> getSettings() {
        SettingsDto settingsDto = settingsService.getSettings();
        return ResponseEntity.ok(settingsDto);
    }
}
