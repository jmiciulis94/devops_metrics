package com.devbridge.internal.devopsmetrics.service;

import com.devbridge.internal.devopsmetrics.dto.AppUserDto;
import com.devbridge.internal.devopsmetrics.dto.AppUserWithPasswordDto;
import com.devbridge.internal.devopsmetrics.dto.RegisterDto;
import com.devbridge.internal.devopsmetrics.exception.CannotDeleteException;
import com.devbridge.internal.devopsmetrics.exception.NotFoundException;
import com.devbridge.internal.devopsmetrics.exception.WrongCredentialsException;
import com.devbridge.internal.devopsmetrics.model.AppUser;
import com.devbridge.internal.devopsmetrics.model.AppUserInfo;
import com.devbridge.internal.devopsmetrics.model.AppUserRole;
import com.devbridge.internal.devopsmetrics.repository.AppUserRepository;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.http.HttpHeaders;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class AppUserService implements UserDetailsService {
    private AppUserRepository appUserRepository;

    public AppUserService(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        Optional<AppUser> appUser = appUserRepository.findByEmailAndDeletedFalse(email);
        if (appUser.isPresent()) {
            String firstName = appUser.get().getFirstName();
            String lastName = appUser.get().getLastName();
            Long id = appUser.get().getId();
            AppUserRole role = appUser.get().getRole();
            return new AppUserInfo(appUser.get().getEmail(), appUser.get().getPassword(), Collections.singletonList(new SimpleGrantedAuthority(role.name())),
                                    firstName, lastName, id);
        } else {
            throw new WrongCredentialsException("Wrong email or password.", "Login");
        }
    }

    public AppUserDto saveUser(RegisterDto registerDto) {
        AppUser appUser = new AppUser(registerDto.getFirstName(),
                                      registerDto.getLastName(),
                                      registerDto.getEmail(),
                                      registerDto.getPassword());
        appUserRepository.save(appUser);
        return new ModelMapper().map(appUser, AppUserDto.class);
    }

    public List<AppUserDto> getUsers(int limit, int page, HttpHeaders headers) {
        String count = String.format("%d", appUserRepository.findAllByDeletedFalse().size());
        headers.add("X-Total-Count", count);
        headers.add("Access-Control-Expose-Headers", "*");
        Pageable pageable = PageRequest.of(page, limit);
        List<AppUser> users = appUserRepository.findAllByDeletedFalse(pageable);
        return new ModelMapper().map(users, new TypeToken<List<AppUserDto>>() {}.getType());
    }

    public void deleteUser (Long id, String email) throws NotFoundException, CannotDeleteException {
        Optional<AppUser> appUser = appUserRepository.findByIdAndDeletedFalse(id);
        if (!appUser.isPresent()){
            throw new NotFoundException(String.format("User with id=%d not found", id), "AppUser");
        } else {
            AppUser user = appUser.get();
            if (user.getEmail().equals(email)) {
                throw new CannotDeleteException("You cannot delete yourself", "Users");
            }
            user.setDeleted(true);
            appUserRepository.save(user);
        }
    }


    public AppUserDto updateUser (AppUserWithPasswordDto appUserWithPasswordDto){
        Optional<AppUser> appUser = appUserRepository.findByIdAndDeletedFalse(appUserWithPasswordDto.getId());
        if (!appUser.isPresent()){
            throw new NotFoundException(String.format("User with id=% not found", appUserWithPasswordDto.getId()), "AppUser");
        } else {
            AppUser user = appUser.get();
            user.setEmail(appUserWithPasswordDto.getEmail());
            user.setFirstName(appUserWithPasswordDto.getFirstName());
            user.setLastName(appUserWithPasswordDto.getLastName());
            if (appUserWithPasswordDto.getPassword()!=null){
                user.setPassword(appUserWithPasswordDto.getPassword());
            }
            appUserRepository.save(user);
            return new ModelMapper().map(user, AppUserDto.class);
        }
    }
   
}



