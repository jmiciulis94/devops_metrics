package com.devbridge.internal.devopsmetrics.model;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

@Getter
public class AppUserInfo extends User {
    private String firstName;
    private String lastName;
    private Long id;

    public AppUserInfo(String username, String password, Collection<? extends GrantedAuthority> authorities, String firstName, String lastName, Long id) {
        super(username, password, authorities);
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
    }
}
