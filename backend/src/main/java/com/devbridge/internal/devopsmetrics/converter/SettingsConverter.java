package com.devbridge.internal.devopsmetrics.converter;

import com.devbridge.internal.devopsmetrics.dto.SettingsDto;
import com.devbridge.internal.devopsmetrics.model.Settings;
import java.math.BigDecimal;
import org.springframework.stereotype.Component;

@Component
public class SettingsConverter {

    public SettingsDto convertEntityToDto(Settings settings) {
        SettingsDto settingsDto = new SettingsDto();
        BigDecimal HUNDRED = new BigDecimal(100);
        settingsDto.setId(settings.getId());
        settingsDto.setDeploymentFrequencyLimit(settings.getDeploymentFrequencyLimit());
        settingsDto.setFailureRateLimit(settings.getFailureRateLimit().multiply(HUNDRED));
        return settingsDto;
    }
}
