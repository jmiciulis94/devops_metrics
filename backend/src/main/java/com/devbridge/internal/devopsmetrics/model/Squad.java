package com.devbridge.internal.devopsmetrics.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;

@Data
@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "squad", schema="devops_metrics")
public class Squad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    @JsonIgnore
    private boolean deleted = false;

    public Squad(String name) { this.name=name; }
}