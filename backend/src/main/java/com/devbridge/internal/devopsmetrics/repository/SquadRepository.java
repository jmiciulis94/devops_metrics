package com.devbridge.internal.devopsmetrics.repository;

import com.devbridge.internal.devopsmetrics.model.Squad;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SquadRepository extends PagingAndSortingRepository<Squad, Long> {
    List<Squad> findAllByDeletedFalseOrderByName();
    List<Squad> findAllByDeletedFalseOrderByName(Pageable pageable);
    Squad findByIdAndDeletedFalse(Long id);
    Boolean existsByNameAndDeletedFalse(String name);
}