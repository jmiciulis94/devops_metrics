package com.devbridge.internal.devopsmetrics.converter;
import com.devbridge.internal.devopsmetrics.dto.ProjectDto;
import com.devbridge.internal.devopsmetrics.dto.ProjectFormDto;
import com.devbridge.internal.devopsmetrics.model.Deployment;
import com.devbridge.internal.devopsmetrics.model.Project;
import com.devbridge.internal.devopsmetrics.repository.SquadRepository;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

@Component
public class ProjectConverter {

    private final SquadRepository squadRepository;

    public ProjectConverter(SquadRepository squadRepository) {
        this.squadRepository = squadRepository;
    }

    public Project convertFormDtoToEntity(ProjectFormDto projectFormDto) {
        Project project = new Project();
        project.setId(projectFormDto.getId());
        project.setTitle(projectFormDto.getTitle());
        if (projectFormDto.getStatus() != null) {
            project.setStatus(projectFormDto.getStatus());
        }
        project.setNotes(projectFormDto.getNotes());
        project.setSquad(squadRepository.findByIdAndDeletedFalse(projectFormDto.getSquadId()));
        return project;
    }

    public ProjectFormDto convertEntityToFormDto(Project project) {
        ProjectFormDto projectFormDto = new ProjectFormDto();
        projectFormDto.setId(project.getId());
        projectFormDto.setTitle(project.getTitle());
        projectFormDto.setStatus(project.getStatus());
        projectFormDto.setNotes(project.getNotes());
        projectFormDto.setSquadId(project.getSquad().getId());
        projectFormDto.setSquadName(project.getSquad().getName());
        return projectFormDto;
    }

    public ProjectDto convertEntityToDto(Project project) {
        ProjectDto projectDto = new ProjectDto();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime lastDeployed = project.getDeployments().stream().filter(deployment -> !deployment.getDeleted()).map(Deployment::getDate).max(LocalDateTime::compareTo).orElse(null);

        projectDto.setId(project.getId());
        projectDto.setTitle(project.getTitle());
        projectDto.setStatus(project.getStatus());
        projectDto.setSquadId(project.getSquad().getId());
        projectDto.setSquadName(project.getSquad().getName());
        projectDto.setNotes(project.getNotes());
        if (lastDeployed != null) {
            String lastDeployedString = formatter.format(lastDeployed);
            projectDto.setLastDeployed(lastDeployedString);
        } else {
            projectDto.setLastDeployed("");
        }
        return projectDto;
    }
}
