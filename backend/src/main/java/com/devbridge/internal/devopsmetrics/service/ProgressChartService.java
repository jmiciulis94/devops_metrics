package com.devbridge.internal.devopsmetrics.service;

import com.devbridge.internal.devopsmetrics.dto.MedianDto;
import com.devbridge.internal.devopsmetrics.dto.ProgressChartDto;
import com.devbridge.internal.devopsmetrics.repository.ProgressChartRepository;
import com.devbridge.internal.devopsmetrics.util.FilteringUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class ProgressChartService {
    private final ProgressChartRepository progressChartRepository;

    public ProgressChartService(ProgressChartRepository progressChartRepository) {
        this.progressChartRepository = progressChartRepository;
    }

    public List<ProgressChartDto> getLeadTime(String from, String to, String period,
                                              List<String>squadIds, List<String>projectIds) {
        List<Integer> squads = FilteringUtil.convertStringArrayToIntArray(squadIds);
        List<Integer> projects = FilteringUtil.convertStringArrayToIntArray(projectIds);
        List<ProgressChartDto> leadTimeRecords;
        List<MedianDto> leadTimeMedianData;
        String metric = "lead_time";
        leadTimeRecords = progressChartRepository.getLeadTime(period, from, to, squads, projects);
        leadTimeMedianData = progressChartRepository.getLeadTimeMedianData(period, from, to, squads, projects);
        calculateMedians(leadTimeRecords, leadTimeMedianData);
        Collections.reverse(leadTimeRecords);
        fillResults(leadTimeRecords, metric);
        return leadTimeRecords;
    }

    public List<ProgressChartDto> getDeploymentDuration(String from, String to, String period,
                                                        List<String>squadIds, List<String>projectIds) {
        List<Integer> squads = FilteringUtil.convertStringArrayToIntArray(squadIds);
        List<Integer> projects = FilteringUtil.convertStringArrayToIntArray(projectIds);
        List<ProgressChartDto> deploymentDurationRecords;
        List<MedianDto> deploymentDurationMedianData;
        String metric = "deployment_duration";
        deploymentDurationRecords = progressChartRepository.getDeploymentDuration(period, from, to, squads, projects);
        deploymentDurationMedianData = progressChartRepository.getDeploymentDurationMedianData(period, from, to, squads, projects);
        calculateMedians(deploymentDurationRecords, deploymentDurationMedianData);
        Collections.reverse(deploymentDurationRecords);
        fillResults(deploymentDurationRecords, metric);
        return deploymentDurationRecords;
    }

    public List<ProgressChartDto> getDeploymentFrequency(String from, String to, String period,
                                                         List<String>squadIds, List<String>projectIds) {
        List<Integer> squads = FilteringUtil.convertStringArrayToIntArray(squadIds);
        List<Integer> projects = FilteringUtil.convertStringArrayToIntArray(projectIds);
        List<ProgressChartDto> deploymentFrequencyRecords;
        List<MedianDto> deploymentFrequencyMedianData;
        if (period.equals("day")) {
            deploymentFrequencyRecords = progressChartRepository.getDeploymentFrequencyDay(from, to, squads, projects);
            Collections.reverse(deploymentFrequencyRecords);
            return deploymentFrequencyRecords;
        } else{
            if (period.equals("month")) {
                deploymentFrequencyRecords = progressChartRepository.getDeploymentFrequencyMonth(from, to, squads, projects);
            } else {
                deploymentFrequencyRecords = progressChartRepository.getDeploymentFrequencyYear(from, to, squads, projects);
            }
            deploymentFrequencyMedianData = progressChartRepository.getDeploymentFrequencyMedianData(period, from, to, squads, projects);
            calculateMedians(deploymentFrequencyRecords, deploymentFrequencyMedianData);
        }
        Collections.reverse(deploymentFrequencyRecords);
        return deploymentFrequencyRecords;
    }

    public List<ProgressChartDto> getFailureRate(String from, String to, String period,
                                                 List<String>squadIds, List<String>projectIds) {
        List<Integer> squads = FilteringUtil.convertStringArrayToIntArray(squadIds);
        List<Integer> projects = FilteringUtil.convertStringArrayToIntArray(projectIds);
        List<ProgressChartDto> failureRateRecords;
        List<MedianDto> failureRateMedianData;
        if (period.equals("day")) {
            failureRateRecords = progressChartRepository.getFailureRate(period, from, to, squads, projects);
        } else {
            failureRateRecords = progressChartRepository.getFailureRate(period, from, to, squads, projects);
            failureRateMedianData = progressChartRepository.getFailureRateMedianData(period, from, to, squads, projects);
            calculateMedians(failureRateRecords, failureRateMedianData);
        }
        Collections.reverse(failureRateRecords);
        return failureRateRecords;
    }

    public void fillResults(List<ProgressChartDto> data, String metric) {
        int i = 0;
        if (data.size() == 0) {
            return;
        }
        if (data.get(0).getAverage() == -1) {
            Float mostRecent = progressChartRepository.getFirst(data.get(0).getDate(), metric);
            data.get(0).setMedian(mostRecent);
            data.get(0).setAverage(mostRecent);
        }
        for (ProgressChartDto record : data) {
            if (record.getAverage() == -1) {
                record.setAverage(data.get(i - 1).getAverage());
                record.setMedian(data.get(i - 1).getMedian());
            }
            i++;
        }
    }

    public List<ProgressChartDto> getLeadTime(String from, String to, String period, Long id) {
        List<ProgressChartDto> leadTimeRecords;
        List<MedianDto> leadTimeMedianData;
        String metric = "lead_time";
        leadTimeRecords = progressChartRepository.getLeadTime(period, from, to, id);
        leadTimeMedianData = progressChartRepository.getLeadTimeMedianData(period, from, to, id);
        calculateMedians(leadTimeRecords, leadTimeMedianData);
        Collections.reverse(leadTimeRecords);
        fillResults(leadTimeRecords, metric, id);
        return leadTimeRecords;
    }

    public List<ProgressChartDto> getDeploymentDuration(String from, String to, String period, Long id) {
        List<ProgressChartDto> deploymentDurationRecords;
        List<MedianDto> deploymentDurationMedianData;
        String metric = "deployment_duration";
        deploymentDurationRecords = progressChartRepository.getDeploymentDuration(period, from, to, id);
        deploymentDurationMedianData = progressChartRepository.getDeploymentDurationMedianData(period, from, to, id);
        calculateMedians(deploymentDurationRecords, deploymentDurationMedianData);
        Collections.reverse(deploymentDurationRecords);
        fillResults(deploymentDurationRecords, metric, id);
        return deploymentDurationRecords;
    }

    public List<ProgressChartDto> getDeploymentFrequency(String from, String to, String period, Long id) {
        List<ProgressChartDto> deploymentFrequencyRecords;
        List<MedianDto> deploymentFrequencyMedianData;
        if (period.equals("day")) {
            deploymentFrequencyRecords = progressChartRepository.getDeploymentFrequencyDay(from, to, id);
            Collections.reverse(deploymentFrequencyRecords);
            return deploymentFrequencyRecords;
        } else if (period.equals("month")) {
            deploymentFrequencyRecords = progressChartRepository.getDeploymentFrequencyMonth(from, to, id);
        } else {
            deploymentFrequencyRecords = progressChartRepository.getDeploymentFrequencyYear(from, to, id);
        }
        deploymentFrequencyMedianData = progressChartRepository.getDeploymentFrequencyMedianData(period, from, to, id);
        calculateMedians(deploymentFrequencyRecords, deploymentFrequencyMedianData);
        Collections.reverse(deploymentFrequencyRecords);
        return deploymentFrequencyRecords;
    }

    public List<ProgressChartDto> getFailureRate(String from, String to, String period, Long id) {
        List<ProgressChartDto> failureRateRecords;
        List<MedianDto> failureRateMedianData;
        failureRateRecords = progressChartRepository.getFailureRate(period, from, to, id);
        failureRateMedianData = progressChartRepository.getFailureRateMedianData(period, from, to, id);
        calculateMedians(failureRateRecords, failureRateMedianData);
        Collections.reverse(failureRateRecords);
        return failureRateRecords;
    }

    public void fillResults(List<ProgressChartDto> data, String metric, Long id) {
        int i = 0;
        if (data.size() == 0) {
            return;
        }
        if (data.get(0).getAverage() == -1) {
            Float mostRecent = progressChartRepository.getFirst(data.get(0).getDate(), metric, id);
            data.get(0).setMedian(mostRecent);
            data.get(0).setAverage(mostRecent);
        }
        for (ProgressChartDto record : data) {
            if (record.getAverage() == -1) {
                record.setAverage(data.get(i - 1).getAverage());
                record.setMedian(data.get(i - 1).getMedian());
            }
            i++;
        }
    }


    public void calculateMedians(List<ProgressChartDto> data, List<MedianDto> medians) {
        int medianIndex = 0;
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Float> mediansArray = new ArrayList<>();
            while (medianIndex < medians.size() && data.get(i).getDate().equals(medians.get(medianIndex).getDate())) {
                if (medians.get(medianIndex).getValue() != (-1)) {
                    mediansArray.add(medians.get(medianIndex).getValue());
                }
                medianIndex++;
            }
            int size = mediansArray.size();
            if (size != 0) {
                float median;
                if (mediansArray.size() % 2 == 0) {
                    median = (mediansArray.get(size / 2) + (float) mediansArray.get(size / 2 - 1)) / 2;
                } else {
                    median = mediansArray.get(size / 2);
                }
                data.get(i).setMedian(median);
            }
        }
    }

}
