package com.devbridge.internal.devopsmetrics.util;

public class SecurityConstants {
    public static final String SECRET_KEY = "67566B59703373367639792442264528482B4D6251655468576D5A7134743777217A25432A462D4A404E635266556A586E3272357538782F413F4428472B4B62";
    public static final long EXPIRATION_TIME = 1000 * 60 * 60; //1 hour
    public static final long TIME_INTERVAL_TO_REFRESH = 1000 * 60 * 10; //10 minutes
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String HEADER_REFRESH_STRING = "Refresh";
    public static final String ACCESS_URL = "/auth/*";
}
