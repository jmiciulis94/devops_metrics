package com.devbridge.internal.devopsmetrics.service;

import com.devbridge.internal.devopsmetrics.converter.SettingsConverter;
import com.devbridge.internal.devopsmetrics.dto.QuadrantDto;
import com.devbridge.internal.devopsmetrics.dto.SettingsDto;
import com.devbridge.internal.devopsmetrics.model.Settings;
import com.devbridge.internal.devopsmetrics.repository.QuadrantRepository;
import com.devbridge.internal.devopsmetrics.repository.SettingsRepository;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;

import com.devbridge.internal.devopsmetrics.util.FilteringUtil;
import org.springframework.stereotype.Service;

@Service
public class QuadrantService {

    private final QuadrantRepository quadrantRepository;
    private final SettingsRepository settingsRepository;
    private final SettingsConverter settingsConverter;

    public QuadrantService(QuadrantRepository quadrantRepository, SettingsRepository settingsRepository, SettingsConverter settingsConverter) {
        this.quadrantRepository = quadrantRepository;
        this.settingsRepository = settingsRepository;
        this.settingsConverter = settingsConverter;
    }

    public List<QuadrantDto> getQuadrantData(String dateFrom, String dateTo, List<String>squadIds, List<String>projectIds) {
        List<Integer> squads = FilteringUtil.convertStringArrayToIntArray(squadIds);
        List<Integer> projects = FilteringUtil.convertStringArrayToIntArray(projectIds);
        Settings settings = settingsRepository.findFirstBy();
        SettingsDto settingsDto = null;
        if (settings != null) {
            settingsDto = settingsConverter.convertEntityToDto(settings);
        }
        BigDecimal HUNDRED = new BigDecimal(100);

        List<QuadrantDto> quadrantDtoList = quadrantRepository.getQuadrantData(dateFrom, dateTo, squads, projects);

        if (settingsDto != null) {
            BigDecimal maxDeploymentFrequency = quadrantDtoList.stream()
                    .map(QuadrantDto::getDeploymentFrequency)
                    .max(Comparator.naturalOrder()).orElse(HUNDRED);

            BigDecimal failRateArgumentField = maxDeploymentFrequency.max(settingsDto.getDeploymentFrequencyLimit());
            QuadrantDto failRateLimitDto1 = new QuadrantDto();
            failRateLimitDto1.setFailRateArgumentField(new BigDecimal(0));
            failRateLimitDto1.setFailRateValueField(settingsDto.getFailureRateLimit());
            QuadrantDto failRateLimitDto2 = new QuadrantDto();
            failRateLimitDto2.setFailRateArgumentField(failRateArgumentField.multiply(new BigDecimal(2)));
            failRateLimitDto2.setFailRateValueField(settingsDto.getFailureRateLimit());

            QuadrantDto deployFreqLimitDto1 = new QuadrantDto();
            deployFreqLimitDto1.setDeployFreqArgumentField(settingsDto.getDeploymentFrequencyLimit());
            deployFreqLimitDto1.setDeployFreqValueField(new BigDecimal(0));
            QuadrantDto deployFreqLimitDto2 = new QuadrantDto();
            deployFreqLimitDto2.setDeployFreqArgumentField(settingsDto.getDeploymentFrequencyLimit());
            deployFreqLimitDto2.setDeployFreqValueField(new BigDecimal(100));

            quadrantDtoList.add(failRateLimitDto1);
            quadrantDtoList.add(failRateLimitDto2);
            quadrantDtoList.add(deployFreqLimitDto1);
            quadrantDtoList.add(deployFreqLimitDto2);
        }

        return quadrantDtoList;
    }

}
