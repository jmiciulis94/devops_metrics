package com.devbridge.internal.devopsmetrics.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class GlobalMetricsDto {
    private List<GlobalMetricsRecordDto> records;
    private List<GlobalMetricsStatDto> stats;
}
