package com.devbridge.internal.devopsmetrics.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "deployment", schema="devops_metrics")
public class Deployment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING)
    private LocalDateTime date;

    @Column(name = "is_defect", nullable = false)
    private Boolean isDefect;

    private String notes;

    @Column(name = "lead_time")
    private Long leadTime;

    @Column(name = "deployment_duration")
    private Long deploymentDuration;

    @Column(nullable = false)
    private Boolean deleted = false;

    @JoinColumn(name = "project_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    @NotNull
    @JsonBackReference
    private Project project;
}