package com.devbridge.internal.devopsmetrics.config;

import com.devbridge.internal.devopsmetrics.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionHandlers {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ValidationMessage> handleNotFoundException(NotFoundException exception) {
        String field = exception.getField();
        String message = exception.getMessage();
        ErrorCode errorCode = ErrorCode.NOT_FOUND;

        ValidationMessage validationMessage = new ValidationMessage(field, message, errorCode);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(validationMessage);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<List<ValidationMessage>> handleValidationException(MethodArgumentNotValidException exception) {
        BindingResult result = exception.getBindingResult();

        List<FieldError> fieldErrors = result.getFieldErrors();

        List<ValidationMessage> messageList = fieldErrors.stream().map(message -> {
            ValidationMessage validationMessage = new ValidationMessage(message.getField(), message.getDefaultMessage(), ErrorCode.FIELD_VALIDATION);
            return validationMessage;
        })
                .collect(Collectors.toList());

        return ResponseEntity.badRequest().body(messageList);
    }

    @ExceptionHandler(WrongCredentialsException.class)
    public ResponseEntity<?> handleWrongCredentialsException(WrongCredentialsException exception) {
        String field = exception.getField();
        String message = exception.getMessage();
        ErrorCode errorCode = ErrorCode.BAD_CREDENTIALS;

        ValidationMessage validationMessage = new ValidationMessage(field, message, errorCode);
        return ResponseEntity.status(HttpStatus.valueOf(400)).body(validationMessage);
    }
        
    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ValidationMessage> handleBadRequestException(BadRequestException exception) {
        String field = exception.getField();
        String message = exception.getMessage();
        ErrorCode errorCode = ErrorCode.BAD_REQUEST;

        ValidationMessage validationMessage = new ValidationMessage(field, message, errorCode);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(validationMessage);
    }

    @ExceptionHandler(CannotDeleteException.class)
    public ResponseEntity<ValidationMessage> handleCannotDeleteException(CannotDeleteException exception) {
        String field = exception.getField();
        String message = exception.getMessage();
        ErrorCode errorCode = ErrorCode.CANNOT_DELETE;

        ValidationMessage validationMessage = new ValidationMessage(field,message,errorCode);
        return ResponseEntity.status(HttpStatus.valueOf(400)).body(validationMessage);
    }

    @ExceptionHandler(RecordAlreadyExistsException.class)
    public ResponseEntity<ValidationMessage> handleAlreadyExistsException(RecordAlreadyExistsException exception) {
        String field = exception.getField();
        String message = exception.getMessage();
        ErrorCode errorCode = ErrorCode.ALREADY_EXISTS;

        ValidationMessage validationMessage = new ValidationMessage(field, message, errorCode);
        return ResponseEntity.status(HttpStatus.valueOf(400)).body(validationMessage);
    }

    class ValidationMessage {
        public String field;
        public String message;
        public ErrorCode errorCode;

        public ValidationMessage(String field, String message, ErrorCode errorCode) {
            this.field = field;
            this.message = message;
            this.errorCode = errorCode;
        }
    }

    enum ErrorCode {
        FIELD_VALIDATION,
        NOT_FOUND,
        BAD_CREDENTIALS,
        BAD_REQUEST,
        CANNOT_DELETE,
        ALREADY_EXISTS
    }
}
