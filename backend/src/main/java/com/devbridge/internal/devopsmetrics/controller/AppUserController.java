package com.devbridge.internal.devopsmetrics.controller;

import com.auth0.jwt.JWT;
import com.devbridge.internal.devopsmetrics.dto.AppUserDto;
import com.devbridge.internal.devopsmetrics.dto.AppUserWithPasswordDto;
import com.devbridge.internal.devopsmetrics.dto.RegisterDto;
import com.devbridge.internal.devopsmetrics.exception.BadRequestException;
import com.devbridge.internal.devopsmetrics.exception.CannotDeleteException;
import com.devbridge.internal.devopsmetrics.exception.NotFoundException;
import com.devbridge.internal.devopsmetrics.service.AppUserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.devbridge.internal.devopsmetrics.util.SecurityConstants.TOKEN_PREFIX;

@RestController
@RequestMapping("/users")
public class AppUserController {
    private AppUserService appUserService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public AppUserController(AppUserService appUserService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.appUserService = appUserService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostMapping
    public ResponseEntity<AppUserDto> saveUser(@RequestBody @Valid RegisterDto registerDto) {
        registerDto.setPassword(bCryptPasswordEncoder.encode(registerDto.getPassword()));
        return ResponseEntity.ok(appUserService.saveUser(registerDto));
    }

    @GetMapping
    public ResponseEntity<List<AppUserDto>> getUsers(@RequestParam("limit") int limit, @RequestParam("page") int page) {
        HttpHeaders headers = new HttpHeaders();
        List<AppUserDto> appUserDtoList = appUserService.getUsers(limit, page, headers);
        return new ResponseEntity<>(appUserDtoList, headers, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
        String email = JWT.decode(token.replace(TOKEN_PREFIX, "")).getSubject();
        try {
            appUserService.deleteUser(id, email);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NotFoundException e) {
            throw e;
        } catch (CannotDeleteException e) {
            throw e;
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<AppUserDto> updateUser(@PathVariable("id") Long id, @RequestBody @Valid AppUserWithPasswordDto appUserWithPasswordDto) {
        if (appUserWithPasswordDto.getId() == id) {
            if (appUserWithPasswordDto.getPassword() != null) {
                appUserWithPasswordDto.setPassword(bCryptPasswordEncoder.encode(appUserWithPasswordDto.getPassword()));
            }
            return ResponseEntity.ok(appUserService.updateUser(appUserWithPasswordDto));
        } else {
            throw new BadRequestException("Path id did not match request object id. Check url.", "User");
        }
    }
}
