package com.devbridge.internal.devopsmetrics.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.devbridge.internal.devopsmetrics.dto.ProgressChartDto;
import com.devbridge.internal.devopsmetrics.dto.ProjectDto;
import com.devbridge.internal.devopsmetrics.dto.ProjectFormDto;
import com.devbridge.internal.devopsmetrics.exception.BadRequestException;
import com.devbridge.internal.devopsmetrics.exception.NotFoundException;
import com.devbridge.internal.devopsmetrics.model.Project;
import com.devbridge.internal.devopsmetrics.service.DeploymentService;
import com.devbridge.internal.devopsmetrics.service.ProgressChartService;
import com.devbridge.internal.devopsmetrics.service.ProjectService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.devbridge.internal.devopsmetrics.util.SecurityConstants.SECRET_KEY;
import static com.devbridge.internal.devopsmetrics.util.SecurityConstants.TOKEN_PREFIX;

@RestController
@RequestMapping(value ="/projects")
public class ProjectController {

    private final ProjectService projectService;
    private final DeploymentService deploymentService;
    private final ProgressChartService progressChartService;

    public ProjectController(ProjectService projectService, ProgressChartService progressChartService, DeploymentService deploymentService) {
        this.projectService = projectService;
        this.progressChartService = progressChartService;
        this.deploymentService = deploymentService;
    }

    @PostMapping
    public ResponseEntity<Project> saveProject(@RequestBody @Valid ProjectFormDto projectFormDto) {
        return ResponseEntity.ok(projectService.saveProject(projectFormDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProjectFormDto> updateProject(@PathVariable(value = "id") Long id, @RequestBody @Valid ProjectFormDto projectFormDto) {
        if (projectFormDto.getId().equals(id)) {
            return ResponseEntity.ok(projectService.updateProject(projectFormDto));
        } else {
            throw new BadRequestException("Path id did not match request object id. Check url.", "Project");
        }
    }

    @GetMapping
    public ResponseEntity<List<ProjectDto>> getProjects(@RequestParam(name = "limit", required = false) Integer limit, @RequestParam(name = "page", required = false) Integer page) {
        HttpHeaders headers = new HttpHeaders();
        List<ProjectDto> projects;
        if (limit == null || page == null) {
            projects = projectService.getProjects();
        } else {
            projects = projectService.getProjects(limit, page, headers);
        }
        return new ResponseEntity<>(projects, headers, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProjectFormDto> getProject(@PathVariable("id") Long id) {
        ProjectFormDto projectFormDto = projectService.getProject(id);
        return ResponseEntity.ok(projectFormDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProject(@PathVariable("id") Long id) {
        Project project = projectService.deleteProject(id);
        if (project != null) {
            deploymentService.deleteDeployments(project.getId());
            return ResponseEntity.noContent().build();
        } else throw new NotFoundException("Could not delete project", "Project");
    }

    @GetMapping(value = "/{id}/progress/lead-time")
    public ResponseEntity<List<ProgressChartDto>> getProgressLeadTimeData(
            @PathVariable("id") Long id,
            @RequestParam(name = "dateFrom", required = false) String from,
            @RequestParam(name = "dateTo", required = false) String to,
            @RequestParam String period) {
        List<ProgressChartDto> leadTimeData;
        if (period == null) {
            throw new BadRequestException("The parameters to get chart are incorrect", "Progress");
        } else {
            leadTimeData = progressChartService.getLeadTime(from, to, period, id);
        }
        return ResponseEntity.ok(leadTimeData);
    }

    @GetMapping(value = "/{id}/progress/deployment-duration")
    public ResponseEntity<List<ProgressChartDto>> getDeploymentDurationData(
            @PathVariable("id") Long id,
            @RequestParam(name = "dateFrom", required = false) String from,
            @RequestParam(name = "dateTo", required = false) String to,
            @RequestParam String period) {
        List<ProgressChartDto> deploymentDurationData;
        if (period == null) {
            throw new BadRequestException("The parameters to get chart are incorrect", "Progress");
        } else {
            deploymentDurationData = progressChartService.getDeploymentDuration(from, to, period, id);
        }
        return ResponseEntity.ok(deploymentDurationData);
    }

    @GetMapping(value = "/{id}/progress/deployment-frequency")
    public ResponseEntity<List<ProgressChartDto>> getProgressDeploymentFrequencyData(
            @PathVariable("id") Long id,
            @RequestParam(name = "dateFrom", required = false) String from,
            @RequestParam(name = "dateTo", required = false) String to,
            @RequestParam String period) {
        List<ProgressChartDto> deploymentFrequencyData;
        if (period == null) {
            throw new BadRequestException("The parameters to get chart are incorrect", "Progress");
        } else {
            deploymentFrequencyData = progressChartService.getDeploymentFrequency(from, to, period, id);
        }
        return ResponseEntity.ok(deploymentFrequencyData);
    }

    @GetMapping(value = "/{id}/progress/failure-rate")
    public ResponseEntity<List<ProgressChartDto>> getProgressFailureRateData(
            @PathVariable("id") Long id,
            @RequestParam(name = "dateFrom", required = false) String from,
            @RequestParam(name = "dateTo", required = false) String to,
            @RequestParam String period) {
        List<ProgressChartDto> failureRateData;
        if (period == null) {
            throw new BadRequestException("The parameters to get chart are incorrect", "Progress");
        } else {
            failureRateData = progressChartService.getFailureRate(from, to, period, id);
        }
        return ResponseEntity.ok(failureRateData);
    }

    @GetMapping("/{id}/deployment-token")
    public ResponseEntity<String> getDeploymentToken(@PathVariable("id") Long id, @RequestHeader("Authorization") String authToken) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(System.currentTimeMillis()));
        cal.add(Calendar.MONTH, 3);
        Date expirationDate = cal.getTime();
        String role = JWT.decode(authToken.replace(TOKEN_PREFIX, "")).getClaim("Role").asString();
        String token = JWT.create()
                .withSubject(Long.toString(id))
                .withExpiresAt(expirationDate)
                .withClaim("Role", role)
                .sign(Algorithm.HMAC512(SECRET_KEY.getBytes()));
        return ResponseEntity.ok(TOKEN_PREFIX + token);
    }

    @GetMapping(params = {"limit", "page"})
    public ResponseEntity<List<ProjectDto>> getProjects(@RequestParam(name = "limit") Integer limit,
                                                        @RequestParam(name = "page") Integer page,
                                                        @RequestParam(name = "dateFrom", required = false) String dateFrom,
                                                        @RequestParam(name = "dateTo", required = false) String dateTo,
                                                        @RequestParam(name = "squadIds", required = false) List<String> squadIds,
                                                        @RequestParam(name = "projectIds", required = false) List<String> projectIds) {
        HttpHeaders headers = new HttpHeaders();
        List<ProjectDto> projects;
        projects = projectService.getProjects(limit, page, dateFrom, dateTo, squadIds, projectIds, headers);
        return new ResponseEntity<>(projects, headers, HttpStatus.OK);
    }
}
