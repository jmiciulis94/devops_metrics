package com.devbridge.internal.devopsmetrics.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.devbridge.internal.devopsmetrics.exception.TokenNeedsRefreshException;
import com.devbridge.internal.devopsmetrics.exception.WrongCredentialsException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static com.devbridge.internal.devopsmetrics.util.SecurityConstants.*;

@RestController
public class AuthorizationFilter extends BasicAuthenticationFilter {

    public AuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            response.addHeader("Access-Control-Expose-Headers", "*");
            String header = request.getHeader(HEADER_STRING);
            if (header == null || !header.startsWith(TOKEN_PREFIX)) {
                chain.doFilter(request, response);
                return;
            }
            Date expirationDate = JWT.decode(header.replace(TOKEN_PREFIX, "")).getExpiresAt();
            long timeLeft = expirationDate.getTime() - new Date(System.currentTimeMillis()).getTime();
            if (timeLeft < TIME_INTERVAL_TO_REFRESH && timeLeft > 0) {
                throw new TokenNeedsRefreshException();
            }

            UsernamePasswordAuthenticationToken authentication = getAuthentication(request);

            SecurityContextHolder.getContext().setAuthentication(authentication);
            chain.doFilter(request, response);
        } catch (TokenExpiredException e) {
            response.setStatus(401);
        } catch (TokenNeedsRefreshException e) {
            response.setStatus(401);
            response.addHeader("X-Needs-Refresh", "true");
        }
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            DecodedJWT decodedJWT = JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                    .build()
                    .verify(token.replace(TOKEN_PREFIX, ""));
            String user = decodedJWT.getSubject();
            String role = decodedJWT.getClaim("Role").asString();

            if (user != null) {
                return new UsernamePasswordAuthenticationToken(user, null, Collections.singletonList(new SimpleGrantedAuthority(role)));
            }
            return null;
        }
        return null;
    }
}
