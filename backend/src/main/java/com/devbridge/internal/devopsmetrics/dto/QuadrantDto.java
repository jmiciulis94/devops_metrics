package com.devbridge.internal.devopsmetrics.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class QuadrantDto {

    private String squadName;
    private String projectTitle;
    private BigDecimal failureRate;
    private BigDecimal deploymentFrequency;
    private BigDecimal deployFreqValueField;
    private BigDecimal deployFreqArgumentField;
    private BigDecimal failRateValueField;
    private BigDecimal failRateArgumentField;

    public QuadrantDto(String squadName, String projectTitle, BigDecimal failureRate, BigDecimal deploymentFrequency) {
        this.squadName = squadName;
        this.projectTitle = projectTitle;
        this.failureRate = failureRate.setScale(4, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
        this.deploymentFrequency = deploymentFrequency.setScale(2, RoundingMode.HALF_UP);
    }
}